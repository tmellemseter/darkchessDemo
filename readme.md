# Darkchess Documentation

## Getting Started
Clone the repo and run `npm install` to download the dependencies.
These are:
* bootstrap
* darkchess
* uniqid
* uuid
* ws
* xorshift

Make sure node.js v4.5.0 is installed then run `node server/server.js` to start the server.
Later versions of node is assumed to work, but not tested.

## Change Log
* 35f8579d  Master Thesis (not cleaned up)
* 543918e8  Fixed default move ordering

## About
Two libraries is needed to implement Darkchess. A client side library for representing a chess board, and a server side library for game play logic. [chessboard.js] is used as the client side library, but extended to distinguish between observable, and unobservable squares. This is achieved by extending the FEN representation. The client side library darkchess.js is *heavily* based on [chess.js].

The [darkchess](https://gitlab.com/tmellemseter/darkchess) library has it's own repo, and can be installed with `npm install --save darkchess`.
The extended _chessboard_ file is located at `assets > js > chessboard.js`.

### chessboard.js (extended)

Simply set `darkMode` to true when instantiating the ChessBoard library.
This will make ChessBoard able to interpret [dark FEN].

Visit [chess.js] to see the original API documentation.

```javascript

let Board = ChessBoard('board', {
    darkMode: true,
    position: '8/8/8/8/8/8/8/8',
    ...
});

```

[chessboard.js]:    http://chessboardjs.com/
[chess.js]:         https://github.com/jhlywa/chess.js/

[dark FEN]:         https://gitlab.com/tmellemseter/darkchess/wikis/Home#fen-and-dark-fen
