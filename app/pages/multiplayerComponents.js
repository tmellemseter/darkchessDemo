'use strict'

if (!ChessBoard) {
    console.error('ERROR: Chessboard library needs to be included');
}

const PIECES = {
    q:  { w: '&#9813;', b: '&#9819' },
    r:  { w: '&#9814;', b: '&#9820' },
    b:  { w: '&#9815;', b: '&#9821' },
    n:  { w: '&#9816;', b: '&#9822' },
    p:  { w: '&#9817',  b: '&#9823' }
};

const Board = {
    id: null,
    load: function(config) {

        if (this.id == null) {
            console.error('ERROR: No id is set.');
            return;
        }

        this.object = ChessBoard(this.id, config);
    },
    reset: function() {
        let config = {
            darkMode: true,
            draggable: false,
            position: '8/8/8/8/8/8/8/8',
            orientation: 'white',
            pieceTheme: 'assets/img/chesspieces/wikipedia/{piece}.png',
        };

        this.object = ChessBoard(this.id, config);
    }
};

const NotationList = {
    animateSpeed: 200,
    list: [],
    selected: null,
    addMove: function(move, highlight) {

        let highlightMove = (highlight === undefined)? true: highlight;
        console.log('highlightMove', highlightMove);

        let itemIndex = this.list.length;
        this.list.push(move);

        //  Assume its a black move being added
        let columnIndex = 3;
        let rowIndex = parseInt(itemIndex/2);

        let isWhite = ((itemIndex % 2) === 0);
        if (isWhite) {
            let newRowHTML  =   '<li id="notation-' + rowIndex + '" class="list-group-item py-1">'
                            +       '<div class="d-flex flex-row notation-row">'
                            +           '<span class="grow-1">' + (rowIndex + 1) + '.</span>'
                            +           '<span class="d-flex grow-4">'
                            +               '<a class="notation-move"></a>'
                            +           '</span>'
                            +           '<span class="d-flex grow-4">'
                            +               '<a class="notation-move"></a>'
                            +           '</span>'
                            +       '</div>'
                            +   '</li>';

            $('#notation').append(newRowHTML);

            $('#notation-wrapper')
                .stop()
                .animate({
                    scrollTop: $('#notation')[0].scrollHeight
                }, 1000);

            columnIndex = 2;
        }

        let setTransparent = (this.selected !== null && highlightMove);
        console.log('st', setTransparent);
        if (setTransparent) {
            this.selected.css('background-color', 'transparent');
        }

        this.selected = $('#notation-' + rowIndex + '> div > span:nth-child(' + columnIndex + ') > a');
        this.selected.html(this.list[itemIndex]);

        if (highlightMove) {
            this.selected.css('background-color', '#ff9');
        }
    },
    setSelection: function(index) {

        if (index >= this.list.length) {
            return;
        }

        let rowIndex = parseInt(index/2);
        let columnIndex = (index % 2) + 2;

        if (this.selected !== null) {
            this.selected.css('background-color', 'transparent');
        }

        this.selected = $('#notation-' + rowIndex + '> div > span:nth-child(' + columnIndex + ') > a');
        this.selected
            .css('background-color', '#ff9')
            .html(this.list[index]);

        let itemTop = $('#notation-' + rowIndex).offset().top;
        let containerTop = $('#notation-wrapper').offset().top;

        let height = itemTop - containerTop + $('#notation-wrapper').scrollTop();
        $('#notation-wrapper').stop().animate({
            scrollTop: height
        }, this.animateSpeed);
    },
    clear: function() {
        this.list = [];
        this.selected = null;

        $('#notation').html('');
    }
}

const PromotionSelector = {
    display: function(target, color, onPromote) {
        let selector = 'div[data-square="' + target + '"]';

        //  SQUARE SIZE:
        let boardElement = $('#' + Board.id);
        let containerWidth = parseInt(boardElement.width(), 10);

        if (!containerWidth || containerWidth <= 0) {
            return 0;
        }

        let boardWidth = containerWidth - 1;

        while (boardWidth % 8 !== 0 && boardWidth > 0) {
            boardWidth--;
        }

        let squareSize = (boardWidth / 8);

        let promoteOptionsHtml =
            '<section id="promotion" class="d-flex flex-column align-items-center">' +
                '<div class="d-flex" data-piece="q">' +
                    '<a class="btn btn-sm cursor-pointer">' +
                        '<img src="assets/img/chesspieces/wikipedia/' + color + 'Q.png"' +
                             'style="width: ' + squareSize + 'px; height: ' + squareSize + 'px;">' +
                    '</a>' +
                '</div>' +
                '<div class="d-flex" data-piece="r">' +
                    '<a class="btn btn-sm cursor-pointer">' +
                        '<img class="img-fuid" src="assets/img/chesspieces/wikipedia/' + color + 'R.png"' +
                             'style="width: ' + squareSize + 'px; height: ' + squareSize + 'px;">' +
                    '</a>' +
                '</div>' +
                '<div class="d-flex" data-piece="n">' +
                    '<a class="btn btn-sm cursor-pointer">' +
                        '<img class="img-fuid" src="assets/img/chesspieces/wikipedia/' + color + 'N.png"' +
                             'style="width: ' + squareSize + 'px; height: ' + squareSize + 'px;">' +
                    '</a>' +
                '</div>' +
                '<div class="d-flex" data-piece="b">' +
                    '<a class="btn btn-sm cursor-pointer">' +
                        '<img src="assets/img/chesspieces/wikipedia/' + color + 'B.png"' +
                             'style="width: ' + squareSize + 'px; height: ' + squareSize + 'px;">' +
                    '</a>' +
                '</div>' +
            '</section>';

        let popoverTemplate =
            '<div class="popover" role="tooltip">' +
                '<div class="popover-arrow"></div>' +
                '<h3 class="popover-title"></h3>' +
                '<div class="popover-content no-padding"></div>' +
            '</div>';

        $(selector).popover({
            title: 'Promote',
            html: true,
            content: $(promoteOptionsHtml),
            placement: 'bottom',
            offset: (squareSize/2) + ' 0',
            template: popoverTemplate
        });

        $(selector).popover('show');
        $('#promotion div').click(function() {
            let piece = $(this).attr('data-piece');
            onPromote(piece);
            $(selector).popover('dispose');
        });

    }
};

const CapturedPieces = {
    TOP: 0,
    BOTTOM: 1,
    state: ['b', 'w'],
    selectors: [
        '#captured-top-pieces',
        '#captured-bottom-pieces',
    ],
    flip: function() {

        let temp = this.state[this.TOP];
        this.state[this.TOP] = this.state[this.BOTTOM];
        this.state[this.BOTTOM] = temp;

        let index = 0;
        this.selectors.forEach((selector) => {
            let pawnCode = PIECES.p[this.state[index]];
            let pawnRow = selector + ' span:first-child > span';
            $(pawnRow).html(pawnCode);

            let officerRow = selector + ' span:last-child';

            let knightCode = PIECES.n[this.state[index]];
            $(officerRow + ' > span:nth-child(1)').html(knightCode);
            $(officerRow + ' > span:nth-child(2)').html(knightCode);

            let bishopCode = PIECES.b[this.state[index]];
            $(officerRow + ' > span:nth-child(3)').html(bishopCode);
            $(officerRow + ' > span:nth-child(4)').html(bishopCode);

            var rookCode = PIECES.r[this.state[index]];
            $(officerRow + ' > span:nth-child(5)').html(rookCode);
            $(officerRow + ' > span:nth-child(6)').html(rookCode);

            let queenCode = PIECES.q[this.state[index]];
            $(officerRow + ' > span:nth-child(7)').html(queenCode);

            index++;
        });
    },
    getState: function(row) {
        return this.state[row];
    },
    setCaptured: function(row, state) {

        let selector = this.selectors[row];
        let color = this.state[row];

        let indexOffset = {
            p: 0,
            n: 0,
            b: 2,
            r: 4,
            q: 6
        };

        let pieces = ['p', 'n', 'b', 'r', 'q'];

        pieces.forEach((piece) => {
            let numberOfCaptured = state[piece];
            let spanChild = 'span:' + ((piece === 'p')? 'first': 'last') + '-child';

            //  NOTE(thomas): Does not consider promotions, but that is ok.
            let maxCaptured = 0;

            switch (piece) {
                case 'p':   maxCaptured = 8;    break;
                case 'q':   maxCaptured = 1;    break;
                default:    maxCaptured = 2;    break;
            }

            for (let i = 0; i < maxCaptured; i++) {
                let index = (i + 1) + indexOffset[piece];
                let pieceSelector = selector + ' ' + spanChild + ' > span:nth-child(' + index + ')';

                if (i < numberOfCaptured) {
                    $(pieceSelector)
                        .html(PIECES[piece][color])
                        .css('visibility', 'visible');
                } else {
                    $(pieceSelector).css('visibility', 'hidden');
                }
            }

        });

    },
    getElements: function() {
        let elements = [];
        this.selectors.forEach((selector) => {
            elements.push(selector + ' span:first-child > span');
            elements.push(selector + ' span:last-child > span');
        });

        return elements;
    }

};
