
let SupportWebSocket = false;
let Socket = {};
let Game = {};

$(document).ready(() => {

    let onNavigateEnd = function() {
        if (Game.history) {
            Game.currentHistoryIndex = Game.latestHistoryIndex;
            NotationList.setSelection(Game.currentHistoryIndex);

            let fen = '';
            switch(Game.activeHistory) {
                case 1:     fen = Game.whiteHistory[Game.currentHistoryIndex];      break;
                case 0:     fen = Game.refereeHistory[Game.currentHistoryIndex];    break;
                case -1:    fen = Game.blackHistory[Game.currentHistoryIndex];      break;
                default:    fen = Game.history[Game.currentHistoryIndex].fen;       break;
            }

            Board.object.position(fen, false);

            let captured = Game.history[Game.currentHistoryIndex].captured;

            let topColor = CapturedPieces.getState(CapturedPieces.TOP);
            CapturedPieces.setCaptured(CapturedPieces.TOP, captured[topColor]);

            let bottomColor = CapturedPieces.getState(CapturedPieces.BOTTOM);
            CapturedPieces.setCaptured(CapturedPieces.BOTTOM, captured[bottomColor]);
        }
    }

    $('#navigate-end').click(onNavigateEnd);
    Input.addAction(Input.ARROW_DOWN, onNavigateEnd);

    let onNavigateStart = function() {
        if (Game.history) {
            Game.currentHistoryIndex = -1;
            NotationList.setSelection(Game.currentHistoryIndex);

            let fen = '';
            switch(Game.activeHistory) {
                case 1:     fen = Game.whiteHistory[Game.currentHistoryIndex];      break;
                case 0:     fen = Game.refereeHistory[Game.currentHistoryIndex];    break;
                case -1:    fen = Game.blackHistory[Game.currentHistoryIndex];      break;
                default:    fen = Game.history[Game.currentHistoryIndex].fen;       break;
            }

            Board.object.position(fen, false);

            let captured = Game.history[Game.currentHistoryIndex].captured;

            let topColor = CapturedPieces.getState(CapturedPieces.TOP);
            CapturedPieces.setCaptured(CapturedPieces.TOP, captured[topColor]);

            let bottomColor = CapturedPieces.getState(CapturedPieces.BOTTOM);
            CapturedPieces.setCaptured(CapturedPieces.BOTTOM, captured[bottomColor]);
        }
    }

    $('#navigate-start').click(onNavigateStart);
    Input.addAction(Input.ARROW_UP, onNavigateStart);

    let onNavigateLeft = function() {
        if (Game.currentHistoryIndex >= 0) {
            NotationList.setSelection(--Game.currentHistoryIndex);

            let fen = '';
            switch(Game.activeHistory) {
                case 1:     fen = Game.whiteHistory[Game.currentHistoryIndex];      break;
                case 0:     fen = Game.refereeHistory[Game.currentHistoryIndex];    break;
                case -1:    fen = Game.blackHistory[Game.currentHistoryIndex];      break;
                default:    fen = Game.history[Game.currentHistoryIndex].fen;       break;
            }

            Board.object.position(fen, false);

            let captured = Game.history[Game.currentHistoryIndex].captured;

            let topColor = CapturedPieces.getState(CapturedPieces.TOP);
            CapturedPieces.setCaptured(CapturedPieces.TOP, captured[topColor]);

            let bottomColor = CapturedPieces.getState(CapturedPieces.BOTTOM);
            CapturedPieces.setCaptured(CapturedPieces.BOTTOM, captured[bottomColor]);

        }
    }

    $('#navigate-left').click(onNavigateLeft);
    Input.addAction(Input.ARROW_LEFT, onNavigateLeft);

    let onNavigateRight = function() {
        if (Game.currentHistoryIndex < Game.latestHistoryIndex) {
            NotationList.setSelection(++Game.currentHistoryIndex);

            let fen = '';
            switch(Game.activeHistory) {
                case 1:     fen = Game.whiteHistory[Game.currentHistoryIndex];      break;
                case 0:     fen = Game.refereeHistory[Game.currentHistoryIndex];    break;
                case -1:    fen = Game.blackHistory[Game.currentHistoryIndex];      break;
                default:    fen = Game.history[Game.currentHistoryIndex].fen;       break;
            }

            Board.object.position(fen, false);

            let captured = Game.history[Game.currentHistoryIndex].captured;

            let topColor = CapturedPieces.getState(CapturedPieces.TOP);
            CapturedPieces.setCaptured(CapturedPieces.TOP, captured[topColor]);

            let bottomColor = CapturedPieces.getState(CapturedPieces.BOTTOM);
            CapturedPieces.setCaptured(CapturedPieces.BOTTOM, captured[bottomColor]);

        }
    }

    $('#navigate-right').click(onNavigateRight);
    Input.addAction(Input.ARROW_RIGHT, onNavigateRight);

    //  Activate all tooltips:
    $('[data-toggle="tooltip"]').tooltip();

    $('#player-id').click(function() {

        let selection = window.getSelection();
        selection.removeAllRanges();

        let range = document.createRange();
        range.selectNodeContents(this);
        selection.addRange(range);

        let success = document.execCommand('copy');
        let feedback = (success)? ['Copied!']: ['Copy with Ctrl-c'];
        $(this).attr('data-original-title', feedback)
               .tooltip('show')
               .attr('data-original-title', 'Copy to Clipboard');

        selection.removeAllRanges();
    });

    // if user is running mozilla then use it's built-in WebSocket
    window.WebSocket = window.WebSocket || window.MozWebSocket;

    SupportWebSocket = ("WebSocket" in window);
    if (!SupportWebSocket) return;

    let isSecure = false;
    let url = CONFIG.getUrl(isSecure);

    Socket = new WebSocket(url);
    Socket.onopen = onOpen;
    Socket.onmessage = onMessage;
    Socket.onclose = onClose;

    Socket.chatSent = false;
    Socket.opponentMuted = false;

    //  Default values:
    Game.playerColor = 'w';
    Game.inGame = false;
    Game.rematchVotes = 0;
    Game.turn = false;

    Game.refereeHistory = [];
    Game.whiteHistory = [];
    Game.blackHistory = [];

    Game.activeHistory = 0;

    Socket.id = null;

    BSC.createButton(BSC.WEBSOCKET_BUTTON, 'cancelChallenge', '#cancel-challenge', {socket: Socket});
    BSC.addAction(BSC.BUTTON, 'cancelChallenge', (self) => {
        Socket.send(CreateMessage(MESSAGE.CANCEL_CHALLENGE, {
            id: Socket.id
        }));

        $('#waiting-block').fadeOut('fast', () => {
            $('#challenge-block').fadeIn('fast');
        });
    });

    BSC.createButton(BSC.WEBSOCKET_BUTTON, 'joinServer', '#join-server-button', {socket: Socket});
    BSC.addAction(BSC.BUTTON, 'joinServer', (self, param) => {

        let message = {};
        if (param) {
            message.id = param.id
        }

        Socket.send(CreateMessage(MESSAGE.JOIN_SERVER, message));
    });

    BSC.createButton(BSC.WEBSOCKET_BUTTON, 'resignGame', '#resign', {socket: Socket});
    BSC.addAction(BSC.BUTTON, 'resignGame', (self) => {
        Socket.send(CreateMessage(MESSAGE.RESIGN, {}));
    });

    Board.id = 'board';
    Board.reset();

    changeServerStatus('info', 'Checking server');

    BSC.createButton(BSC.WEBSOCKET_BUTTON, 'sendChallenge', '#send-challenge-button', {socket: Socket});
    BSC.addAction(BSC.BUTTON, 'sendChallenge', () => {

        let colorValue = BSC.getValue(BSC.GROUP, 'selectColor');
        let opponentID = $('#opponent-id-input').val();

        if (opponentID === '') {
            console.warn('WARNING: Opponent ID is missing');
            return;
        }

        Socket.send(CreateMessage(MESSAGE.SEND_CHALLENGE, {
            color: colorValue,
            id: opponentID
        }));

        $('#challenge-block').fadeOut('fast', () => {

            $('#waiting-section')
                .css('display', 'flex')
                .show();

            $('#declined-section')
                .css('display', 'flex')
                .hide();

            $('#accept-section')
                .css('display', 'flex')
                .hide();

            $('#busy-section')
                .css('display', 'flex')
                .hide();

            $('#waiting-block')
                .css('display', 'flex')
                .hide()
                .fadeIn('fast');

        });

    });

    BSC.createGroup('panelTabs');
    BSC.addElement('panelTabs', 'game', '#games-tab', '#game-tab-content');
    BSC.addElement('panelTabs', 'play', '#play-tab', '#play-tab-content');
    BSC.addElement('panelTabs', 'profile', '#profile-tab', '#profile-tab-content');
    BSC.setActive('panelTabs', 'profile');
    BSC.addAction(BSC.GROUP, 'panelTabs', (previousActive, active) => {

        $(previousActive).fadeOut('fast', () => {
            $(active).fadeIn('fast');
        });
    });

    BSC.createGroup('selectColor');
    BSC.addElement('selectColor', 'white', '#challenge-color-white');
    BSC.addElement('selectColor', 'random', '#challenge-color-random');
    BSC.addElement('selectColor', 'black', '#challenge-color-black');
    BSC.setActive('selectColor', 'random');
    BSC.addAction(BSC.GROUP, 'selectColor', (prev, next) => {

        if (next === '#challenge-color-random') {
            return;
        }

        if (prev !== next) {
            let bottomColor = CapturedPieces.getState(CapturedPieces.BOTTOM);
            let nextColor = (next === '#challenge-color-white')? 'w': 'b';

            if (bottomColor !== nextColor) {
                CapturedPieces.flip();
                Board.object.flip();
            }
        }

    });

    BSC.createButton(BSC.WEBSOCKET_BUTTON, 'rematch', '#rematch', {socket: Socket});
    BSC.addAction(BSC.BUTTON, 'rematch', () => {
        Socket.send(CreateMessage(MESSAGE.REMATCH, {}));
    });

    BSC.createButton(BSC.SIMPLE_BUTTON, 'decline', '#declined-ok');
    BSC.addAction(BSC.BUTTON, 'decline', () => {
        $('#waiting-block').fadeOut('fast', () => {
            $('#challenge-block').fadeIn('fast');

            $('#waiting-section')
                .css('display', 'flex')
                .show();

            $('#declined-section')
                .css('display', 'flex')
                .hide();

            $('#accept-section')
                .css('display', 'flex')
                .hide();

            $('#busy-section')
                .css('display', 'flex')
                .hide();
        });
    });

    BSC.createButton(BSC.SIMPLE_BUTTON, 'busy', '#busy-ok');
    BSC.addAction(BSC.BUTTON, 'busy', () => {

        $('#waiting-block').fadeOut('fast', () => {
            $('#challenge-block').fadeIn('fast');

            $('#waiting-section')
                .css('display', 'flex')
                .show();

            $('#declined-section')
                .css('display', 'flex')
                .hide();

            $('#accept-section')
                .css('display', 'flex')
                .hide();

            $('#busy-section')
                .css('display', 'flex')
                .hide();
        });

    });

    BSC.createButton(BSC.WEBSOCKET_BUTTON, 'startGame', '#start-game-button', {socket: Socket});
    BSC.addAction(BSC.BUTTON, 'startGame', () => {
        Socket.send(CreateMessage(MESSAGE.START_GAME, {}));

        $('#waiting-block').fadeOut('fast', () => {
            $('#challenge-block').fadeIn('fast');

            $('#waiting-section')
                .css('display', 'flex')
                .show();

            $('#declined-section')
                .css('display', 'flex')
                .hide();

            $('#accept-section')
                .css('display', 'flex')
                .hide();

            $('#busy-section')
                .css('display', 'flex')
                .hide();
        });
    });

    BSC.createButton(BSC.SIMPLE_BUTTON, 'rotateBoard', '#rotate-board');
    BSC.addAction(BSC.BUTTON, 'rotateBoard', () => {
        CapturedPieces.flip();
        Board.object.flip();
    });

    BSC.createButton(BSC.SIMPLE_BUTTON, 'blackDarkMode', '#black-darkmode');
    BSC.addAction(BSC.BUTTON, 'blackDarkMode', () => {
        $('#referee-darkmode-active').css('visibility', 'hidden');
        $('#white-darkmode-active').css('visibility', 'hidden');
        $('#black-darkmode-active').css('visibility', 'visible');

        Game.activeHistory = -1;

        let fen = Game.blackHistory[Game.currentHistoryIndex];
        Board.object.position(fen, false);
    });

    BSC.createButton(BSC.SIMPLE_BUTTON, 'noneDarkMode', '#referee-darkmode');
    BSC.addAction(BSC.BUTTON, 'noneDarkMode', () => {
        $('#black-darkmode-active').css('visibility', 'hidden');
        $('#white-darkmode-active').css('visibility', 'hidden');
        $('#referee-darkmode-active').css('visibility', 'visible');

        Game.activeHistory = 0;

        let fen = Game.refereeHistory[Game.currentHistoryIndex];
        Board.object.position(fen, false);
    });

    BSC.createButton(BSC.SIMPLE_BUTTON, 'whiteDarkMode', '#white-darkmode');
    BSC.addAction(BSC.BUTTON, 'whiteDarkMode', () => {
        $('#black-darkmode-active').css('visibility', 'hidden');
        $('#referee-darkmode-active').css('visibility', 'hidden');
        $('#white-darkmode-active').css('visibility', 'visible');

        Game.activeHistory = 1;

        let fen = Game.whiteHistory[Game.currentHistoryIndex];
        Board.object.position(fen, false);
    });

    BSC.createButton(BSC.WEBSOCKET_BUTTON, 'downloadPGN', '#download-pgn', {socket: Socket});
    BSC.addAction(BSC.BUTTON, 'downloadPGN', () => {
        Socket.send(CreateMessage(MESSAGE.GET_PGN_CONTENT, {}));
    });

    let chatOptionsElement = $('<div class="d-flex justify-content-between" style="width:150px"></div>');
    BSC.createGroup('chatRow', chatOptionsElement, (content) => {
        if (!Game.inGame || Socket.chatSent) return;
        Socket.chatSent = true;

        Socket.send(CreateMessage(MESSAGE.CHAT, {
            content: content
        }));

        $('#player-icon').popover('hide');

        onDisplayChatIcon('#player-icon span', content, 'fa-user');

        setTimeout(function() {
            Socket.chatSent = false;
        }, 4000);
    });

    let smileIcon = $('<span class="fa fa-smile-o fa-3x cursor-pointer"></span>');
    BSC.addElement('chatRow', 'smileButton', smileIcon, 'smile');

    let mehIcon = $('<span class="fa fa-meh-o fa-3x cursor-pointer"></span>');
    BSC.addElement('chatRow', 'mehButton', mehIcon, 'meh');

    let frownIcon = $('<span class="fa fa-frown-o fa-3x cursor-pointer"></span>');
    BSC.addElement('chatRow', 'frownButton', frownIcon, 'frown');

    //  NOTE(thomas):
    //  This is not really a group, but eh, it works OK.

    let muteElement = $('<div class="d-flex justify-content-between"></div>');
    BSC.createGroup('muteRow', muteElement, (content) => {
        if (!Game.inGame) return;

        Socket.send(CreateMessage(MESSAGE.MUTE, {}));

        Socket.opponentMuted = !Socket.opponentMuted;
        if (Socket.opponentMuted) {

            $('#opponent-icon span').fadeOut('fast', function() {
                $(this).removeClass('fa-user')
                       .addClass('fa-user-times')
                       .fadeIn('fast');
                $('#mute-button').html('Unmute');
                $('#opponent-icon').popover('hide');
            });

        } else {
            $('#opponent-icon span').fadeOut('fast', function() {
                $(this).removeClass('fa-user-times')
                       .addClass('fa-user')
                       .fadeIn('fast');
                $('#mute-button').html('Mute');
                $('#opponent-icon').popover('hide');
            });
        }
    });

    let muteIcon = $('<button id="mute-button" class="btn btn-sm btn-default cursor-pointer">Mute</button>');
    BSC.addElement('muteRow', 'muteButton', muteIcon, 'mute');

});

function onOpen() {
    console.log('New connection');
    BSC.disable(BSC.BUTTON, 'joinServer', false);

    changeServerStatus('success', 'Online');
}

function onMessage(response) {

    let message = JSON.parse(response.data);

    switch(message.type) {
        case MESSAGE.JOIN_SERVER:       joinServer(message.content);            break;
        case MESSAGE.RECIEVE_CHALLENGE: recieveChallenge(message.content);      break;
        case MESSAGE.ACCEPT_CHALLENGE:  acceptChallenge();                      break;
        case MESSAGE.DECLINE_CHALLENGE: declineChallenge();                     break;
        case MESSAGE.CANCEL_CHALLENGE:  cancelChallenge();                      break;
        case MESSAGE.START_GAME:        startGame(message.content);             break;
        case MESSAGE.ON_DROP:           onDropResponse(message.content);        break;
        case MESSAGE.RESIGN:            onResignation(message.content);         break;
        case MESSAGE.BUSY:              onBusy();                               break;
        case MESSAGE.REMATCH:           onRematch();                            break;
        case MESSAGE.GET_PGN_CONTENT:   onRetrievePGNContent(message.content);  break;
        case MESSAGE.CHAT:              onChatRecieve(message.content);         break;
        default: console.error('Message type not handled: ' + message.type);
    }

    if (message.feedback) {

        //  NOTE(thomas):
        //  A bit strange that all messages can provide
        //  feedback from the referee, no?

        $('#feedback-banner')
            .html(message.feedback)
            .delay(100)
            .fadeIn(100)
            .fadeOut(100)
            .fadeIn(100)
            .fadeOut(100)
            .fadeIn(100);
    }
}

function joinServer(content) {
    Socket.id = content.id;
    console.log('ID', Socket.id);

    $('.player-id').html(Socket.id);

    $('#join-server-card').fadeOut('fast', function() {
        $('#in-server-card').fadeIn('fast');
    });
}

function recieveChallenge(content) {

    $('#feedback-id').html(content.id);
    $('#feedback-ok').hide();

    if (content.color === 'Random') {
        $('#feedback-certain-colors').hide();
        $('#feedback-random-colors').show();
    } else {
        $('#feedback-random-colors').hide();

        let feedbackColorMessage = (content.color === 'White')? 'I': 'You';
        feedbackColorMessage += ' will start with White pieces.';
        $('#feedback-certain-colors').html(feedbackColorMessage);

        $('#feedback-certain-colors').show();
    }

    $('#feedback-yes').click(() => {
        Socket.send(CreateMessage(MESSAGE.ACCEPT_CHALLENGE, {
            id: content.id
        }));
        $('#feedback-card').fadeOut('fast');
    });

    $('#feedback-no').click(() => {
        Socket.send(CreateMessage(MESSAGE.DECLINE_CHALLENGE, {
            id: content.id
        }));
        $('#feedback-card').fadeOut('fast');
    });

    $('#feedback-card').fadeIn();
}

function acceptChallenge() {

    $('#waiting-section').fadeOut('fast', () => {
        $('#accept-section').fadeIn('fast');
    });

    $('#feedback-card').fadeOut('fast');
}

function declineChallenge() {

    $('#waiting-section').fadeOut('fast', () => {
        $('#declined-section').fadeIn('fast');
    });

    $('#feedback-card').fadeOut('fast');
}

function cancelChallenge() {

    $('#feedback-card p').html('Challenge has been cancelled.');
    $('#feedback-yes').hide();
    $('#feedback-no').hide();
    $('#feedback-ok').show();
    $('#feedback-ok').click(() => {
        $('#feedback-card').fadeOut('fast');
    });
}

function startGame(content) {
    console.log('Start New Game');

    BSC.setPopover('muteRow', '#opponent-icon');
    BSC.setPopover('chatRow', '#player-icon');

    $('#opponent-icon').addClass('cursor-pointer');
    $('#player-icon').addClass('cursor-pointer');

    $('#player-icon')
        .mouseover(function() {
            $('#player-icon span').removeClass('fa-user')
                   .addClass('fa-comment')
        })
        .mouseleave(function() {
            $('#player-icon span').removeClass('fa-comment')
                   .addClass('fa-user')
        });

    CapturedPieces.getElements().forEach((element) => {
        $(element).css('visibility', 'hidden');
    });

    $('#rematch').fadeOut('fast', () => {
        $(this).html(' Rematch (0/2)');
        $('#resign').fadeIn('fast');
    });

    BSC.setActive('panelTabs', 'game');

    $('#notation-navigation')
        .css('display', 'flex')
        .fadeIn('fast');

    $('#white-darkmode-active').css('visibility', 'hidden');
    $('#referee-darkmode-active').css('visibility', 'hidden');
    $('#black-darkmode-active').css('visibility', 'hidden');
    $('#analyze-navigation').fadeOut('fast');

    //  TODO(thomas):
    //  Improve highlighting, less annoying.
    //  Maybe a simple yellowish highlight that blinks instead?

    $('#notation-wrapper').fadeIn();
    $('#feedback-banner')
        .html('Game has started. White to begin')
        .delay(100)
        .fadeIn(100)
        .fadeOut(100)
        .fadeIn(100)
        .fadeOut(100)
        .fadeIn(100);

    content.config.onDragStart = onDragStart;
    content.config.onDrop = onDrop;

    content.config.pieceTheme = 'assets/img/chesspieces/wikipedia/{piece}.png';

    Board.load(content.config);

    $('#opponent-id').html(content.config.opponent);

    let historyEntry = {
        fen: content.config.position,
        captured: content.captured,
    };

    Game.inGame = true;
    Game.isFinished = false;

    Game.history = [];
    Game.history[-1] = historyEntry;

    Game.whiteHistory = [];
    Game.refereeHistory = [];
    Game.blackHistory = [];

    Game.activeHistory = null;
    Game.currentHistoryIndex = 0;
    Game.latestHistoryIndex = 0;

    Game.turn = (content.config.orientation === 'white');
    Game.playerColor = (content.config.orientation === 'white')? 'w': 'b';

    let bottomColor = CapturedPieces.getState(CapturedPieces.BOTTOM);
    if (bottomColor === 'w' && Game.playerColor === 'b') {
        CapturedPieces.flip();
    }

    NotationList.clear();
}

function onBusy() {
    console.log('Client is busy');

    $('#waiting-section').fadeOut('fast', () => {
        $('#busy-section').fadeIn('fast');
    });
}

function onRematch() {
    $('#rematch').html(' Rematch (1/2)');
}

function onRetrievePGNContent(content) {

    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(content.pgn));
    element.setAttribute('download', 'pgn');

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

function onChatRecieve(content) {

    console.log('Chat recieved', content);
    onDisplayChatIcon('#opponent-icon span', content, 'fa-user');
}

function onDisplayChatIcon(iconSelector, iconId, defaultClass) {

    let iconClass = defaultClass;
    switch(iconId) {
        case 'smile':   iconClass = "fa-smile-o";   break;
        case 'meh':     iconClass = "fa-meh-o";     break;
        case 'frown':   iconClass = "fa-frown-o";   break;
    }

    $(iconSelector).fadeOut('fast', function() {
        $(this).removeClass(defaultClass)
               .addClass(iconClass)
               .fadeIn('fast');
    });

    setTimeout(function() {
        $(iconSelector).fadeOut('fast', function() {
            $(this).removeClass(iconClass)
                   .addClass(defaultClass)
                   .fadeIn('fast');
            });
    }, 4000);
}

function makeMove(move) {

    Socket.send(CreateMessage(MESSAGE.ON_DROP, {
        move: move
    }));

    Game.turn = false;
}

function onDragStart() {
    return true;
}

function onDrop(source, target, piece) {

    let myPiece = (Game.playerColor === piece[0]);
    if (!Game.turn || !myPiece || Game.isFinished) {
        return 'snapback';
    }

    if (source === target) return;

    let isPawn = (Board.object.position()[source][1] === 'P');

    let promotionRank = (Game.playerColor === 'w')? '8': '1';
    let pawnPromotion = (target[1] === promotionRank);

    let move = {
        from: source,
        to:   target,
        promotion: null
    };

    if (isPawn && pawnPromotion) {

        PromotionSelector.display(target, Game.playerColor, (piece) => {
            move.promotion = piece;
            makeMove(move);
        });
    } else {
        makeMove(move);
    }

    return true;
}

function onDropResponse(state) {

    //  Lock drag-and-drop when not your turn.
    Game.turn = (Game.playerColor === state.turn);

    if (state.fen) {
        Board.object.position(state.fen, false);

        if (state.isValidMove) {

            let historyEntry = {
                fen: state.fen,
                captured: state.captured,
            };

            Game.history.push(historyEntry);
            Game.latestHistoryIndex = (Game.history.length - 1);
            Game.currentHistoryIndex = Game.latestHistoryIndex;
        }
    }

    if (!state.isValidMove) return;

    if (state.san) {
        NotationList.addMove(state.san);
    }

    let topColor = CapturedPieces.getState(CapturedPieces.TOP);
    CapturedPieces.setCaptured(CapturedPieces.TOP, state.captured[topColor]);

    let bottomColor = CapturedPieces.getState(CapturedPieces.BOTTOM);
    CapturedPieces.setCaptured(CapturedPieces.BOTTOM, state.captured[bottomColor]);

    if (state.isGameOver) {
        endGame(state);
    }
}

function endGame(state) {
    Game.isFinished = true;
    Game.inGame = false;
    Game.turn = false;

    if (state.endSan) {
        NotationList.addMove(state.endSan, false);
    }

    for (let i = 0; i < state.refereeHistory.length; i++) {
        Game.refereeHistory[i - 1] = state.refereeHistory[i];
        Game.whiteHistory[i - 1] = state.whiteHistory[i];
        Game.blackHistory[i - 1] = state.blackHistory[i];
    }

    $('#opponent-icon').removeClass('cursor-pointer');
    $('#player-icon').removeClass('cursor-pointer');

    $('#player-icon').unbind('mouseover');
    $('#player-icon').unbind('mouseleave');

    $('#resign').fadeOut('fast', () => {
        $('#rematch')
            .html(' Rematch (0/2)')
            .fadeIn('fast');
    });

    $('#analyze-navigation')
        .css('display', 'flex')
        .fadeIn('fast');

    if (Game.playerColor === 'w') {
        $('#white-darkmode-active').css('visibility', 'visible');
    } else {
        $('#black-darkmode-active').css('visibility', 'visible');
    }
}

function onResignation(state) {
    console.log('Resign Game');
    endGame(state);
}

function onClose() {
    console.log('Close');
    BSC.disable(BSC.BUTTON, 'joinServer', true);

    changeServerStatus('danger', 'Server is offline');

}

function changeServerStatus(severity, message) {
    $('#server-status').fadeOut(function() {

        BSC.changeStyle(this, 'badge', severity);
        $(this).html(message);
        $(this).fadeIn();
    });
}
