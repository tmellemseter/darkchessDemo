
let SupportWebSocket = false;
let Socket;
let Game = {};

/*
    $(window).resize(function() {
        $(this).resize(Board.object.resize);
    });
*/

$(document).ready(function() {
    $('.card-deck').fadeIn();

    let onNavigateEnd = function() {
        if (Game.history) {
            Game.currentHistoryIndex = Game.latestHistoryIndex;
            NotationList.setSelection(Game.currentHistoryIndex);

            let history = Game.history[Game.currentHistoryIndex];
            Board.object.position(history.fen, false);

            let topColor = CapturedPieces.getState(CapturedPieces.TOP);
            CapturedPieces.setCaptured(CapturedPieces.TOP, history.captured[topColor]);

            let bottomColor = CapturedPieces.getState(CapturedPieces.BOTTOM);
            CapturedPieces.setCaptured(CapturedPieces.BOTTOM, history.captured[bottomColor]);
        }
    }

    $('#navigate-end').click(onNavigateEnd);
    Input.addAction(Input.ARROW_DOWN, onNavigateEnd);

    let onNavigateStart = function() {
        if (Game.history) {
            Game.currentHistoryIndex = -1;
            NotationList.setSelection(Game.currentHistoryIndex);
            let history = Game.history[Game.currentHistoryIndex];
            Board.object.position(history.fen, false);

            let topColor = CapturedPieces.getState(CapturedPieces.TOP);
            CapturedPieces.setCaptured(CapturedPieces.TOP, history.captured[topColor]);

            let bottomColor = CapturedPieces.getState(CapturedPieces.BOTTOM);
            CapturedPieces.setCaptured(CapturedPieces.BOTTOM, history.captured[bottomColor]);
        }
    }

    $('#navigate-start').click(onNavigateStart);
    Input.addAction(Input.ARROW_UP, onNavigateStart);

    let onNavigateLeft = function() {
        if (Game.currentHistoryIndex >= 0) {
            NotationList.setSelection(--Game.currentHistoryIndex);
            let history = Game.history[Game.currentHistoryIndex];
            Board.object.position(history.fen, false);

            let topColor = CapturedPieces.getState(CapturedPieces.TOP);
            CapturedPieces.setCaptured(CapturedPieces.TOP, history.captured[topColor]);

            let bottomColor = CapturedPieces.getState(CapturedPieces.BOTTOM);
            CapturedPieces.setCaptured(CapturedPieces.BOTTOM, history.captured[bottomColor]);
        }
    }

    $('#navigate-left').click(onNavigateLeft);
    Input.addAction(Input.ARROW_LEFT, onNavigateLeft);

    let onNavigateRight = function() {
        if (Game.currentHistoryIndex < Game.latestHistoryIndex) {
            NotationList.setSelection(++Game.currentHistoryIndex);
            let history = Game.history[Game.currentHistoryIndex];
            Board.object.position(history.fen, false);

            let topColor = CapturedPieces.getState(CapturedPieces.TOP);
            CapturedPieces.setCaptured(CapturedPieces.TOP, history.captured[topColor]);

            let bottomColor = CapturedPieces.getState(CapturedPieces.BOTTOM);
            CapturedPieces.setCaptured(CapturedPieces.BOTTOM, history.captured[bottomColor]);
        }
    }

    $('#navigate-right').click(onNavigateRight);
    Input.addAction(Input.ARROW_RIGHT, onNavigateRight);

    // If user is running mozilla - then use it's built-in WebSocket
    window.WebSocket = window.WebSocket || window.MozWebSocket;

    SupportWebSocket = ("WebSocket" in window);
    if (!SupportWebSocket) {
        $('#greeting-row').fadeIn();
        return;
    }

    $('#app-row').fadeIn();

    let isSecure = false;
    let url = CONFIG.getUrl(isSecure);

    Socket = new WebSocket(url);
    Socket.onopen = onOpen;
    Socket.onmessage = onMessage;
    Socket.onclose = onClose;

    Game.playerColor = 'w';
    Game.inGame = false;

    Board.id = 'board';
    Board.reset();

    BSC.createButton(BSC.RADIO_BUTTON, 'selectColor', '#start-game-options input');
    BSC.addAction(BSC.BUTTON, 'selectColor', (buttonId) => {
        let color = $(buttonId).val();
        let selectedColor = (color === 'white')? 'w': 'b';

        if (Game.playerColor === selectedColor) return;
        Game.playerColor = selectedColor;

        Board.object.flip();
        CapturedPieces.flip();
    });

    BSC.createButton(BSC.WEBSOCKET_BUTTON, 'startGame', '#start-game-button', {socket: Socket});
    BSC.addAction(BSC.BUTTON, 'startGame', () => {

        $('#start-game-container').fadeOut();
        $('#notation-navigation').css('visibility', 'visible');

        let color = BSC.getValue(BSC.RADIO_BUTTON, 'selectColor');

        Game.playerColor = (color === 'white')? 'w': 'b';
        Game.inPlay = true;

        Board.reset(Game.playerColor);

        //GUI.emptyElement('#notation');

        Socket.send(CreateMessage(MESSAGE.START_GAME, {
            color: Game.playerColor
        }));

    });

    $('#start-game-options input').click(function() {
        let color = $(this).val();
        let selectedColor = (color === 'white')? 'w': 'b';

        if (Game.playerColor === selectedColor) return;
        Game.playerColor = selectedColor;

        Board.object.flip();
        CapturedPieces.flip();
    });


    $('#resign').click(function() {
        let message = CreateMessage(MESSAGE.RESIGN, {});
        Socket.send(message);
    });

    $('#games-link').click(function() {
        $('#games-link').addClass('active');
        $('#tests-link').removeClass('active');

        $('#notation-block').css('display', 'block');
        $('#tests-block').css('display', 'none');
    });
});

function onOpen() {
    $('#start-game-button').attr('disabled', false);
    changeServerStatus('success', 'Server is online');
}

function onMessage(response) {
    let message = JSON.parse(response.data);

    switch(message.type) {
        case MESSAGE.START_GAME:             startGame(message.content);         break;
        case MESSAGE.ON_DROP:                onDropResponse(message.content);    break;
        case MESSAGE.RESIGN:                 onResignResponse(message.content);  break;
        default: console.error('Message type not handled: ' + message.type);
    }

    if (message.feedback) {
        $('#feedback-banner').html(message.feedback);
        $('#feedback-banner').fadeIn();
    }
}

function onClose(response) {
    if (response.code != 1000) {
        if (!navigator.onLine) {
            // TODO(thomas): Display a proper feedback message.
        }
    }
    $('#start-game-button').attr('disabled', true);
    changeServerStatus('danger', 'Server is offline');
}

function onDragStart(source, piece, position, orientation) {
    let myPiece = Game.playerColor === piece[0];
    return (Game.turn && myPiece && Game.bothKingsOnBoard);
};

function makeMove(move) {
    let message = CreateMessage(MESSAGE.ON_DROP, {
        move: move
    });

    Socket.send(message);
    Game.turn = false;
}

function onDrop(source, target) {

    if (source === target) return;

    let isPawn = (Board.object.position()[source][1] === 'P');
    let promotionRank = (Game.playerColor === 'w')? '8': '1';
    let pawnPromotion = (target[1] === promotionRank);

    let move = {
        from: source,
        to: target,
        promotion: null
    };

    if (isPawn && pawnPromotion) {

        move.promotion = 'q';
        makeMove(move);

    } else {
        makeMove(move);
    }

    return true;
};

function startGame(config) {

    CapturedPieces.getElements().forEach((element) => {
        $(element).css('visibility', 'hidden');
    });

    config.onDragStart = onDragStart;
    config.onDrop = onDrop;

    config.pieceTheme = 'assets/img/chesspieces/wikipedia/{piece}.png';

    Board.load(config);

    let captured = {
        b: {p: 0, k: 0, b: 0, r: 0, q: 0},
        w: {p: 0, k: 0, b: 0, r: 0, q: 0},
    };

    let historyEntry = {
        fen: config.position,
        captured: captured,
    };

    Game.history = [];
    Game.history[-1] = historyEntry;

    Game.currentHistoryIndex = 0;
    Game.latestHistoryIndex = 0;
    Game.inGame = true;

    Game.turn = (config.orientation === 'white');
    Game.playerColor = (config.orientation === 'white')? 'w': 'b';
    Game.isFinished = false;

    NotationList.clear();
}

function showCapturedPiece(color, type, number) {

    let playerSection = (color === Game.playerColor)? '#captured-bottom-pieces': '#captured-top-pieces';

    let pawnRow = playerSection + ' span:first-child';

    if (type === 'p') {
        $(pawnRow + '> span:nth-child(' + number + ')').css('visibility', 'visible');
    }

    let officerRow = playerSection + ' span:last-child';

    /*  NOTE(thomas):
        type is simply an index in the row, following this structure.

        +-----------+-----------+-----------+
        | type      | number    | nth value |
        +-----------+-----------+-----------+
        | knight    | 1         | 1         |
        | knight    | 2         | 2         |
        | bishop    | 1         | 3         |
        | bishop    | 2         | 4         |
        | rook      | 1         | 5         |
        | rook      | 2         | 6         |
        | queen     | 1         | 7         |
        +-----------+-----------+-----------+

        knight = number
        bishop = number + 2
        rook = number + 4
        queen = 7
    */



}

function onDropResponse(state) {

    // Lock drag and drop when not your turn.
    Game.turn = (Game.playerColor === state.turn);

    // Update board position also when invalid moves have been made.
    if (state.fen) {
        Board.object.position(state.fen, false);

        if (state.isValidMove) {

            let historyEntry = {
                fen: state.fen,
                captured: state.captured,
            };

            Game.history.push(historyEntry);
            Game.latestHistoryIndex = (Game.history.length - 1);
            Game.currentHistoryIndex = Game.latestHistoryIndex;
        }
    }

    // Prevent adding invalid moves to the notation list.
    if (!state.isValidMove) return;

    NotationList.addMove(state.san);

    let topColor = CapturedPieces.getState(CapturedPieces.TOP);
    CapturedPieces.setCaptured(CapturedPieces.TOP, state.captured[topColor]);

    let bottomColor = CapturedPieces.getState(CapturedPieces.BOTTOM);
    CapturedPieces.setCaptured(CapturedPieces.BOTTOM, state.captured[bottomColor]);

    if (state.isGameOver) {
        Game.isFinished = true;
        Game.inGame = false;

        $('#start-game-container').fadeIn();
        $('#resign').css('visibility', 'hidden');
    }
}

function onResignResponse(state) {
    Game.isFinished = true;
    Game.inGame = false;

    Board.object.position(state.fen, false);
    Game.history.push(state.fen);

    Game.latestHistoryIndex = (Game.history.length - 1);
    Game.currentHistoryIndex = Game.latestHistoryIndex;

    NotationList.addMove(state.san);
}

function changeServerStatus(severity, statusMessage) {

    $('#server-status').fadeOut(function() {
        changeIndicator(this, 'badge', severity);
        $(this).html(statusMessage);
        $(this).fadeIn();
    });
}

function changeIndicator(elementSelector, component, severityToAdd) {
    let severities = ['default', 'primary', 'success', 'info', 'warning', 'danger'];
    severities = severities.map(function(severity) {
        return (component + '-' + severity);
    });

    let severityAllreadyPresent = false;

    severities.forEach(function(severity) {
        if (severity === severityToAdd) {
            return;
        }

        if ($(elementSelector).hasClass(severity)) {
            $(elementSelector).removeClass(severity);
        }
    });

    let severityClass = (component + '-' + severityToAdd);

    if (!$(elementSelector).hasClass(severityClass)) {
        $(elementSelector).addClass(severityClass);
    }
}

$(window).on('unload', function() {
    if (Socket) {
        Socket.onclose = function() {};
        Socket.close(1000, "Deliberate disconnection");
    }
});
