'use strict'

let Input = (function() {

    const ARROW_LEFT = 37;
    const ARROW_UP = 38;
    const ARROW_RIGHT = 39;
    const ARROW_DOWN = 40;

    let actions = [];

    $(document).keydown((event) => {

        if (actions[event.which] !== undefined) {
            event.preventDefault();

            actions[event.which].forEach((action) => {
                action();
            });
        }

    });

    function addAction(keyDown, action) {
        actions[keyDown] = [];
        actions[keyDown].push(action);
    }

    let API = {
        ARROW_LEFT: ARROW_LEFT,
        ARROW_UP: ARROW_UP,
        ARROW_RIGHT: ARROW_RIGHT,
        ARROW_DOWN: ARROW_DOWN,

        addAction: addAction,
    };

    return API;

})();
