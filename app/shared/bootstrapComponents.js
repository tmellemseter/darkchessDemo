'use strict'

let BSC = (() => {

    const BUTTON = 0;
    const GROUP = 1;

    const STYLES = ['default', 'primary', 'success', 'info', 'warning', 'danger'];

    const SIMPLE_BUTTON =  0;
    const WEBSOCKET_BUTTON =  1;
    const RADIO_BUTTON = 2;

    let groups = [];
    let buttons = [];

    function changeStyle(selector, component, styleToAdd) {

        let styleClass = (component + '-' + styleToAdd);

        STYLES.forEach((style) => {
            if (style === styleToAdd) {
                return;
            }

            let currentClass = (component + '-' + style);
            if ($(selector).hasClass(currentClass)) {
                $(selector).removeClass(currentClass);
            }
        });

        if (!$(selector).hasClass(styleClass)) {
            $(selector).addClass(styleClass);
        }
    }

    function createButton(type, name, id, option) {

        let button = {
            type: type,
            id: id,
            actions: []
        };

        if (type === WEBSOCKET_BUTTON) {

            if (!option) {
                console.error('Error: Option parameter is needed with the socket');
            }

            if (option && !('socket' in option)) {
                console.error('ERROR: WebSocket needs to be provided via the options parameter');
                return;
            }

            button.actions.push(() => {
                if (option.socket.readyState !== window.WebSocket.OPEN) {
                    console.error('ERROR: Connection is not open');
                    return false;
                }
            });
        }

        $(id).click(function() {
            for (let i = 0; i < button.actions.length; i++) {
                let action = button.actions[i];
                let value = action(this);
                if (value === false) break;
            }
        });

        buttons[name] = button;
    }

    function createGroup(name, element, action) {

        //  Common group properties
        let group = {
            name: name,
            elements: []
        };

        let isDynamic = (typeof element === "object" && element !== null);

        if (isDynamic) {
            //  "Dynamic" group properties
            group.element = element || null;
            group.action = action || null;

        } else {
            //  Content group properties
            group.active = null;
            group.actions = [];
            group.contents = [];
            group.contentsLength = 0;
        }

        groups[name] = group;
    }

    function addAction(type, name, action) {

        switch (type) {
            case BUTTON: {
                buttons[name].actions.push(action);
            } break;
            case GROUP:{
                let group = groups[name];

                //  NOTE(thomas):
                //  Not intended for "dynamic" groups...
                if (!group.actions) {
                    console.error("Wrong group type");
                    return;
                }

                group.actions.push(action);
            } break;
            default:        console.error('ERROR: ' + type + ' not supported!');
        }

    }

    function addElement(groupName, elementName, elementSource, elementContent) {
        let group = groups[groupName];
        group.elements[elementName] = elementSource;

        //  If "Dynamic" group, only initialize click event, skip everything else.
        if (typeof elementSource === "object" && elementSource != null) {
            elementSource.click(() => {
                group.action(elementContent);
            });

            group.element.append(elementSource);
            return;
        }

        if (elementContent) {
            group.contents[elementName] = elementContent;
            group.contentsLength++;
        }

        $(elementSource).click(function() {
            $(group.elements[group.active]).removeClass('active');
            $(this).addClass('active');

            group.actions.forEach((action) => {

                if (elementContent) {
                    action(group.contents[group.active], group.contents[elementName]);
                } else {
                    action(group.elements[group.active], group.elements[elementName]);
                }
            });

            group.active = elementName;
        });
    }

    function getValue(type, name) {

        let value = null;

        switch(type) {
            case RADIO_BUTTON: {
                let button = buttons[name];
                value = $(button.id + '[type="radio"]:checked').val();
            } break;
            case GROUP: {
                let group = groups[name];

                if (!group.active) {
                    console.error("Wrong group type");
                }

                value = $(group.elements[group.active]).html();
            } break;
            default:        console.error('ERROR: ' + type + ' not supported!');  break;
        }

        return value;
    }

    function disable(type, name, flag) {

        let id = null;

        switch (type) {
            case BUTTON:    id = buttons[name].id;  break;
            case GROUP:     /*  TODO(thomas)    */  break;
            default:        console.error('ERROR: ' + type + ' not supported!');
        }

        $(id).attr('disabled', flag);
    }

    function setActive(groupName, elementName) {

        let group = groups[groupName];

        if (!group.contents) {
            console.error("Wrong group type");
            return;
        }

        $(group.elements[group.active]).removeClass('active');
        if (group.contentsLength > 0) {
            $(group.contents[group.active]).fadeOut('fast');
        }

        group.active = elementName;
        $(group.elements[group.active]).addClass('active');
        if (group.contentsLength > 0) {
            $(group.contents[elementName]).fadeIn('fast');
        }
    }

    function click(name, param) {
        buttons[name].actions.forEach((action) => {
            action(buttons[name].id, param);
        });
    }

    function setPopover(groupName, selector) {
        $(selector).popover({
            content: groups[groupName].element,
            html: true
        });
    }

    let API = {
        BUTTON:             BUTTON,
        GROUP:              GROUP,

        RADIO_BUTTON:       RADIO_BUTTON,
        SIMPLE_BUTTON:      SIMPLE_BUTTON,
        WEBSOCKET_BUTTON:   WEBSOCKET_BUTTON,

        changeStyle:        changeStyle,
        createButton:       createButton,
        createGroup:        createGroup,
        addAction:          addAction,
        addElement:         addElement,
        setPopover:         setPopover,

        click:              click,
        disable:            disable,
        setActive:          setActive,

        getValue:           getValue
    };

    return API;
})();
