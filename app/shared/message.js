const MESSAGE = {
    START_GAME: 0,
    ON_DROP: 1,
    RESIGN: 2,
    JOIN_SERVER: 3,
    ACCEPT_CHALLENGE: 4,
    SEND_CHALLENGE: 5,
    RECIEVE_CHALLENGE: 6,
    CANCEL_CHALLENGE: 7,
    DECLINE_CHALLENGE: 8,
    BUSY: 9,
    REMATCH: 10,
    GET_PGN_CONTENT: 11,
    CHAT: 12,
    MUTE: 13
};

const MAX_MESSAGES = 14;

let CreateMessage = (function() {
    'use strict';

    return function(type, content) {

        if (typeof content !== "object" || "type" in content) {
            console.error('Second parameter MUST be an object without a TYPE property');
            return;
        }

        let messageTypeChecked = 0;
        for (let key in MESSAGE) {
            if (MESSAGE.hasOwnProperty(key)) {
                if (type === MESSAGE[key]) break;
            }
            messageTypeChecked++;
        }

        if (messageTypeChecked === MAX_MESSAGES) {
            console.error("Error: Message type " + type + " is not supported.");
            return;
        }

        content.type = type;
        return JSON.stringify(content);
    };
})();
