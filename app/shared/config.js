
let CONFIG = (() => {

    let HOST = 'www.localhost';
    let PORT = 8080;

    return {
        getUrl: function(isSecure) {
            let url = 'ws' + ((isSecure)? 's': '') + '://' + HOST + ':' + PORT;
            return url;
        }
    };
})();
