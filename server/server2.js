'use strict'

/*  TODO(THOMAS):

    [ ] Test on Firefox

    ****************************************************************

    [ ] Fix AI implementation to the new Darkchess API

    [ ] Invite notification - refactoring (list) own tab?

    [ ] Refactor Feedback messages from Referee
            Flash / indicate new messsages
            Sound / toggle on | off under Profile?

    [ ] In-game and about to invite / challenge another:
        Swapping color, becomes opposite if previously black...

    [ ] Better ID generator

    ****************************************************************
*/

const WebSocket = require('ws');
const Http = require('http');
const UniqID = require('uniqid');

const Darkchess = require('../node_modules/darkchess/darkchess');

const PORT = 8080;

const MESSAGE = {
    START_GAME: 0,
    ON_DROP: 1,
    RESIGN: 2,
    JOIN_SERVER: 3,
    ACCEPT_CHALLENGE: 4,
    SEND_CHALLENGE: 5,
    RECIEVE_CHALLENGE: 6,
    CANCEL_CHALLENGE: 7,
    DECLINE_CHALLENGE: 8,
    BUSY: 9,
    REMATCH: 10,
    GET_PGN_CONTENT: 11,
    CHAT: 12,
    MUTE: 13
};

const WEBSOCKET_STATUS = {
    CONNECTING: 0,
    OPEN: 1,
    CLOSING: 2,
    CLOSED: 3
};

function getText(message) {
    for (let key in MESSAGE) {
        if (MESSAGE[key] === message) {
            return key;
        }
    }

    return '-----';
}

let Sockets = [];

let Server = Http.createServer((request, response) => {
    //  NOTE(thomas):
    //  We only care for a WebSocket server, this should be empty.
});

Server.listen(PORT, () => {
    console.log('Server is listening on port ' + PORT);
});

let verify = function(info) {
    console.log('Origin: ' + info.origin);

    //  TODO(thomas):
    //  Return true only when orgin comes from darkchess.computer
    //  Test what info.origin is on the server...

    return true;
}

let WSS = new WebSocket.Server({
    server: Server,
    verifyClient: verify
});

let RefereeHistory = [];
let WhiteHistory = [];
let BlackHistory = [];

WSS.on('connection', (socket) => {

    setDefaultState(socket);

    socket.Send = function(object, callback) {
        if (this.readyState === WEBSOCKET_STATUS.OPEN) {
            this.send(JSON.stringify(object), callback);
        } else {
            console.log('Socket is NOT open');
        }
    }

    socket.on('pong', () => {   socket.isActive = true;  });

    socket.on('message', (request) => {

        let message = JSON.parse(request);

        let typeText = getText(message.type);
        console.log('Message recieved: ' + typeText);

        switch(message.type) {
            case MESSAGE.JOIN_SERVER:       joinServer(socket, message);        break;
            case MESSAGE.SEND_CHALLENGE:    sendChallenge(socket, message);     break;
            case MESSAGE.ACCEPT_CHALLENGE:  acceptChallenge(socket, message);   break;
            case MESSAGE.DECLINE_CHALLENGE: declineChallenge(socket, message);  break;
            case MESSAGE.CANCEL_CHALLENGE:  cancelChallenge(socket);            break;
            case MESSAGE.START_GAME:        startGame(socket);                  break;
            case MESSAGE.ON_DROP:           onDrop(socket, message.move);       break;
            case MESSAGE.RESIGN:            onResign(socket, true);             break;
            case MESSAGE.REMATCH:           onRematch(socket);                  break;
            case MESSAGE.GET_PGN_CONTENT:   onGetPGNContent(socket);            break;
            case MESSAGE.CHAT:              onChatSend(socket, message);        break;
            case MESSAGE.MUTE:              onToggleMute(socket);               break;
            default: console.log('ERROR: Message type not supported');
        }
    });

    socket.on('close', () => {
        console.log('On Close');
    });
});

function setDefaultState(socket) {

    socket.uid = null;
    socket.isActive = true;
    socket.chatSent = false;
    socket.isMuted = false;

    socket.gameState = {
        challenges: [],
        numberOfChallenges: 0,

        toMove: false,
        playerColor: Darkchess.WHITE,
        inGame: false,
        referee: null,
        voteSent: false,
        opponent: null,
        captured: null
    };
}

function setDefaultChallengeState(socket, whiteId, blackId, keepCurrent) {

    let challenge = {
        gameNumber: -1,
        isActive: true,
        white:  whiteId,
        black:  blackId,
        rematchVotes: 0,
    };

    socket.gameState.challenges.push(challenge);

    if (!keepCurrent) {
        socket.gameState.numberOfChallenges++;
    }
}

function joinServer(socket, content) {
    console.log('New connection');

    socket.uid = UniqID.time();
    Sockets[socket.uid] = socket;

    let message = {
        type: MESSAGE.JOIN_SERVER,
        content: {
            id: socket.uid
        }
    };

    socket.Send(message);
}

function sendChallenge(socket, content) {

    if (content.id === '') {
        console.log('WARNING: ID is empty');
        return;
    }

    if (!Sockets[content.id]) {
        console.log('WARNING: Invalid client id');
        return;
    }

    let gameState = Sockets[content.id].gameState;
    if (gameState.inGame) {
        socket.Send({
            type: MESSAGE.BUSY,
        });
        return;
    }

    // Save color if Random is selected.
    let messageColor = content.color;

    if (content.color === 'Random') {
        content.color = (Math.random() > 0.5)? 'White': 'Black';
    }

    let whiteId = (content.color === 'White')? socket.uid: content.id;
    let blackId = (content.color === 'Black')? socket.uid: content.id;
    setDefaultChallengeState(socket, whiteId, blackId, socket.gameState.inGame);

    let message = {
        type: MESSAGE.RECIEVE_CHALLENGE,
        content: {
            color: messageColor,
            id: socket.uid
        }
    };

    Sockets[content.id].Send(message);

    //  For making the challenge inActive when finished, preventing incomming moves after finished.

    let id = Sockets[content.id].gameState.numberOfChallenges - 1;
    let challengerId = socket.gameState.numberOfChallenges - 1;
    Sockets[content.id].gameState.challenges[id] = socket.gameState.challenges[challengerId];
}

function acceptChallenge(socket, content) {
    console.log('\nAccept challenge');

    let message = {};

    let id = Sockets[content.id].gameState.numberOfChallenges - 1;
    if (!Sockets[content.id].gameState.challenges[id].isActive) {
        message.type = MESSAGE.CANCEL_CHALLENGE;
    } else {
        message.type = MESSAGE.ACCEPT_CHALLENGE;
    }

    Sockets[content.id].Send(message);
}

function declineChallenge(socket, content) {
    console.log('\nDecline challenge');

    let message = {
        type: MESSAGE.DECLINE_CHALLENGE
    };

    Sockets[content.id].Send(message);
}

function cancelChallenge(socket) {
    let id = socket.gameState.numberOfChallenges - 1;

    if (socket.gameState.challenges[id]) {
        socket.gameState.challenges[id].isActive = false;
    }
}

function startGame(socket) {

    if (socket.gameState.inGame) {
        console.log('Trying to start game when allready in a game!');
        onResign(socket, false);

        socket.gameState.numberOfChallenges++;
    }

    let id = socket.gameState.numberOfChallenges - 1;
    let challenge = socket.gameState.challenges[id];
    challenge.isActive = true;

    //  The state defaults to white perspective
    let referee = new Darkchess();
    referee.setState(Darkchess.DEFAULT_POSITION);

    let config = {
        darkMode: true,
        draggable: true,
        position: referee.getDarkFen(Darkchess.WHITE),
        orientation: Darkchess.getColorWord(Darkchess.WHITE).toLowerCase(),
        opponent: challenge.black
    };

    RefereeHistory = [];
    RefereeHistory.push(referee.getFen());

    WhiteHistory = [];
    WhiteHistory.push(referee.getDarkFen(Darkchess.WHITE));

    BlackHistory = [];
    BlackHistory.push(referee.getDarkFen(Darkchess.BLACK));

    let captured = {};
    captured[Darkchess.WHITE] = {};
    captured[Darkchess.WHITE][Darkchess.PAWN] = 0;
    captured[Darkchess.WHITE][Darkchess.KNIGHT] = 0;
    captured[Darkchess.WHITE][Darkchess.BISHOP] = 0;
    captured[Darkchess.WHITE][Darkchess.ROOK] = 0;
    captured[Darkchess.WHITE][Darkchess.QUEEN] = 0;

    captured[Darkchess.BLACK] = {};
    captured[Darkchess.BLACK][Darkchess.PAWN] = 0;
    captured[Darkchess.BLACK][Darkchess.KNIGHT] = 0;
    captured[Darkchess.BLACK][Darkchess.BISHOP] = 0;
    captured[Darkchess.BLACK][Darkchess.ROOK] = 0;
    captured[Darkchess.BLACK][Darkchess.QUEEN] = 0;

    let message = {
        type: MESSAGE.START_GAME,
        content: {
            config: config,
            captured: captured,
        }
    };

    Sockets[challenge.white].Send(message);

    Sockets[challenge.white].gameState.toMove = true;
    Sockets[challenge.white].gameState.playerColor = Darkchess.WHITE;
    Sockets[challenge.white].gameState.inGame = true;
    Sockets[challenge.white].gameState.referee = referee;
    Sockets[challenge.white].gameState.voteSent = false;
    Sockets[challenge.white].gameState.opponent = Sockets[challenge.black];
    Sockets[challenge.white].gameState.captured = captured;

    let whiteId = Sockets[challenge.white].gameState.numberOfChallenges - 1;
    Sockets[challenge.white].gameState.challenges[whiteId] = challenge;

    config.position = referee.getDarkFen(Darkchess.BLACK);
    config.orientation = Darkchess.getColorWord(Darkchess.BLACK).toLowerCase();
    config.opponent = challenge.white;

    message.content.config = config;
    Sockets[challenge.black].Send(message);
    Sockets[challenge.black].gameState.toMove = false;
    Sockets[challenge.black].gameState.playerColor = Darkchess.BLACK;
    Sockets[challenge.black].gameState.inGame = true;
    Sockets[challenge.black].gameState.referee = referee;
    Sockets[challenge.black].gameState.voteSent = false;
    Sockets[challenge.black].gameState.opponent = Sockets[challenge.white];
    Sockets[challenge.black].gameState.captured = captured;

    let blackId = Sockets[challenge.black].gameState.numberOfChallenges - 1;
    Sockets[challenge.black].gameState.challenges[blackId] = challenge;
}

function onResign(socket, offerRematch) {

    let id = socket.gameState.numberOfChallenges - 1;
    socket.gameState.challenges[id].isActive = false;

    let winner = Darkchess.getColorWord(socket.gameState.playerColor);

    let response = {
        type: MESSAGE.RESIGN,
        content: {
            refereeHistory: RefereeHistory,
            whiteHistory: WhiteHistory,
            blackHistory: BlackHistory,
            endSan: (socket.gameState.playerColor === Darkchess.WHITE)? '0 - 1': '1 - 0',
        },
        feedback: winner + ' won by resignation.',
    };

    socket.Send(response);
    socket.gameState.opponent.Send(response);
}

function onDrop(socket, move) {

    let id = socket.gameState.numberOfChallenges - 1;

    if (!socket.gameState.challenges[id].isActive) {
        console.log('Challenge not active.');
        return;
    }

    let move0x88 = socket.gameState.referee.isValidMove(move);
    let isValidMove = (socket.gameState.toMove && (move0x88 !== null));

    let response = {
        type: MESSAGE.ON_DROP,
        content: {
            isValidMove: isValidMove,
            fen: (socket.gameState.toMove)? socket.gameState.referee.getDarkFen(socket.gameState.playerColor): null,
            turn: socket.gameState.playerColor,
            captured: socket.gameState.captured,
            isGameOver: false,
            san: null,
        },
        feedback: (socket.gameState.toMove)? 'Illegal move': 'Opponent to move!'
    };

    if (!isValidMove) {

        //  Assure correct board state on client side, in case of illegal move attempts.
        socket.Send(response);
        return;
    }

    let opponent = socket.gameState.opponent;

    if (move0x88.captured) {
        opponent.gameState.captured[opponent.gameState.playerColor][move0x88.captured]++;
    }

    socket.gameState.referee.move(move0x88);
    socket.gameState.toMove = false;

    RefereeHistory.push(socket.gameState.referee.getFen());
    WhiteHistory.push(socket.gameState.referee.getDarkFen(Darkchess.WHITE));
    BlackHistory.push(socket.gameState.referee.getDarkFen(Darkchess.BLACK));

    response.content.san = Darkchess.getSAN(move0x88);
    response.content.fen = socket.gameState.referee.getDarkFen(socket.gameState.playerColor);
    response.content.turn = socket.gameState.referee.getTurn();
    response.content.captured = socket.gameState.captured;

    response.feedback = 'Opponent is thinking';

    let isGameOver = socket.gameState.referee.isGameOver();
    if (isGameOver) {
        console.log('Game Is Over!');

        let san = '1/2 - 1/2';
        let gameOverCondition = '';
        if (socket.gameState.referee.isInsufficientMaterial()) {
            gameOverCondition = 'Draw by insufficient material.';

        } else if (socket.gameState.referee.hit50MoveRule()) {
            gameOverCondition = 'Draw by 50 move rule.';

        } else {

            san = '0 - 1';
            let winner = 'Black';

            if (socket.gameState.referee.getWinner() === Darkchess.WHITE) {
                winner = 'White';
                san = '1 - 0';
            }

            gameOverCondition = winner + ' won by capture.';
        }

        RefereeHistory.push(socket.gameState.referee.getFen());
        WhiteHistory.push(socket.gameState.referee.getDarkFen(Darkchess.WHITE));
        BlackHistory.push(socket.gameState.referee.getDarkFen(Darkchess.BLACK));

        response.content.endSan = san;
        response.content.isGameOver = true;
        response.feedback = gameOverCondition;

        response.content.refereeHistory = RefereeHistory;
        response.content.whiteHistory = WhiteHistory;
        response.content.blackHistory = BlackHistory;

        socket.gameState.inGame = false;
        socket.gameState.opponent.inGame = false;

        socket.Send(response);

        let opponentColor = Darkchess.swapColor(socket.gameState.playerColor);
        response.content.fen = socket.gameState.referee.getDarkFen(opponentColor);
        socket.gameState.opponent.Send(response);

        let id = socket.gameState.numberOfChallenges - 1;
        socket.gameState.challenges[id].isActive = false;
        return;
    }

    socket.Send(response);

    let opponentColor = Darkchess.swapColor(socket.gameState.playerColor);
    response.content.fen = socket.gameState.referee.getDarkFen(opponentColor);
    response.content.san = socket.gameState.referee.getSAN(move0x88, opponentColor);
    response.feedback = 'Your turn';

    opponent.Send(response);
    opponent.gameState.toMove = true;
}

function onRematch(socket) {

    if (!socket.gameState.voteSent) {
        socket.gameState.voteSent = true;

        let id = socket.gameState.numberOfChallenges - 1;
        let challenge = socket.gameState.challenges[id];

        challenge.rematchVotes++;

        console.log('Votes: ' + challenge.rematchVotes);

        if (challenge.rematchVotes === 2) {

            socket.gameState.inGame = false;
            challenge.isActive = true;
            challenge.rematchVotes = 0;

            let temp = challenge.white;
            challenge.white = challenge.black;
            challenge.black = temp;

            startGame(socket);
        } else {

            let response = {
                type: MESSAGE.REMATCH,
            };

            socket.Send(response);
            socket.gameState.opponent.Send(response);
        }
    } else {
        console.log('Vote sent!');
    }
}

function onGetPGNContent(socket) {
    let message = {
        type: MESSAGE.GET_PGN_CONTENT,
        content: {
            pgn: socket.gameState.referee.saveToPGN()
        }
    };

    socket.Send(message);
}

function onChatSend(socket, message) {

    let notInGame = !socket.gameState.inGame;
    let opponentHasMuted = (socket.gameState.opponent && socket.gameState.opponent.isMuted);

    if (notInGame || socket.chatSent || opponentHasMuted) return;
    socket.chatSent = true;

    let response = {
        type: MESSAGE.CHAT,
        content: message.content
    };

    socket.gameState.opponent.Send(response);

    setTimeout(function() {
        socket.chatSent = false;
    }, 4000);
}

function onToggleMute(socket) {
    if (!socket.gameState.inGame) return;
    socket.isMuted = !socket.isMuted;
}

const interval = setInterval(() => {
    WSS.clients.forEach((client) => {
        if (!client.isActive) {
            let index = Sockets.indexOf(client.uid);
            Sockets.splice(index, 1);
            return client.terminate();
        }

        client.isActive = false;
        client.ping('', false, true);
    });
}, 2000);
