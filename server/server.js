'use strict'

const WebSocketServer = require('ws').Server;
const Http = require('http');

const Fs = require('fs');
const UuidV1 = require('uuid/v1');

const MESSAGE = {
    START_GAME: 0,
    ON_DROP: 1,
    RESIGN: 2,
    JOIN_SERVER: 3,
    ACCEPT_CHALLENGE: 4,
    SEND_CHALLENGE: 5,
    RECIEVE_CHALLENGE: 6,
    CANCEL_CHALLENGE: 7,
    DECLINE_CHALLENGE: 8,
    BUSY: 9,
    REMATCH: 10,
};

const WEBSOCKET_STATUS = {
    CONNECTING: 0,
    OPEN: 1,
    CLOSING: 2,
    CLOSED: 3
};

const Darkchess = require('../node_modules/darkchess/darkchess');
const Agent = require('./ais/agent2');

let testID = process.argv[2];
let fileName = process.argv[3];
let userTesting = (typeof testID !== 'undefined' && typeof fileName !== 'undefined');

if (userTesting) {
    console.log('\n\tUSER TESTING');
    console.log('\tID: ', testID);
    console.log('\tFile', fileName, '\n');
}

let WebSocketServerPort = 8080;
let Computer = {};

let Server = Http.createServer(function(request, response) {
    // NOTE(thomas): Do not need this for a WebSocket Server.
});

Server.listen(WebSocketServerPort, function() {
    console.log("Server is listening on port " + WebSocketServerPort);
});

let WSServer = new WebSocketServer({ server: Server });

WSServer.on('connection', function(socket) {

    socket.Send = function(object, callback) {
        if (this.readyState === WEBSOCKET_STATUS.OPEN) {
            this.send(JSON.stringify(object), callback);
        } else {
            console.log('Socket is NOT open');
        }
    }

    socket.on('message', function(request) {

        let message = JSON.parse(request);

        switch(message.type) {
            case MESSAGE.START_GAME:    startGame(socket, message.color);   break;
            case MESSAGE.ON_DROP:       onDrop(socket, message.move);       break;
            case MESSAGE.RESIGN:        onResignation(socket, message);     break;
            default: console.log('Unsupported message type recieved: ' + message.type);
        };

    });

    socket.on('close', () => {
        console.log('Close');
    });

});

function setDefaultState(socket) {

    let captured = {};
    captured[Darkchess.WHITE] = {};
    captured[Darkchess.WHITE][Darkchess.PAWN] = 0;
    captured[Darkchess.WHITE][Darkchess.KNIGHT] = 0;
    captured[Darkchess.WHITE][Darkchess.BISHOP] = 0;
    captured[Darkchess.WHITE][Darkchess.ROOK] = 0;
    captured[Darkchess.WHITE][Darkchess.QUEEN] = 0;

    captured[Darkchess.BLACK] = {};
    captured[Darkchess.BLACK][Darkchess.PAWN] = 0;
    captured[Darkchess.BLACK][Darkchess.KNIGHT] = 0;
    captured[Darkchess.BLACK][Darkchess.BISHOP] = 0;
    captured[Darkchess.BLACK][Darkchess.ROOK] = 0;
    captured[Darkchess.BLACK][Darkchess.QUEEN] = 0;

    socket.gameState = {
        playerColor: null,
        isPlaying: false,
        toMove: false,
        resigned: false,
        referee: {},
        captured: captured
    };

}

function startGame(socket, color) {

    setDefaultState(socket);

    if (socket.gameState.isPlaying) return;
    socket.gameState.isPlaying = true;

    socket.gameState.referee = new Darkchess();
    socket.gameState.referee.setState(Darkchess.DEFAULT_POSITION);

    socket.gameState.playerColor = color;
    socket.gameState.toMove = (socket.gameState.playerColor === Darkchess.WHITE);

    let opponentColor = Darkchess.swapColor(socket.gameState.playerColor);

    Computer = new Agent('Computer');
    Computer.setColor(opponentColor);
    Computer.setState(Darkchess.DEFAULT_POSITION);
    Computer.enableRisk();
    Computer.sortMoves();
    Computer.onlyBestmoves();
    Computer.setSearchDepth(2);

    let orientation = Darkchess.getColorWord(socket.gameState.playerColor);

    let config = {
        darkMode: true,
        draggable: true,
        position: socket.gameState.referee.getDarkFen(socket.gameState.playerColor),
        orientation: orientation.toLowerCase()
    };

    let response = {
        type: MESSAGE.START_GAME,
        content: config,
        feedback: 'White to begin.'
    };

    socket.Send(response);

    if (opponentColor === Darkchess.WHITE) {

        let move0x88 = Computer.move();

        socket.gameState.referee.move(move0x88);

        let responseMove = {
            type: MESSAGE.ON_DROP,
            content: {
                isValidMove: true,
                fen: socket.gameState.referee.getDarkFen(socket.gameState.playerColor),
                san: socket.gameState.referee.getSAN(move0x88, socket.gameState.playerColor),
                captured: socket.gameState.captured,
                turn: Darkchess.BLACK,
                isGameOver: false,
            },
            feedback: 'Your move'
        };

        socket.Send(responseMove);
        socket.gameState.toMove = true;
        if (socket.gameState.resigned) resigning(socket);

    }
}

function onDrop(socket, move) {

    //let referee = socket.gameState.referee;

    let move0x88 = socket.gameState.referee.isValidMove(move);
    let isValidMove = (socket.gameState.toMove && (move0x88 !== null));

    let response = {
        type: MESSAGE.ON_DROP,
        content: {
            isValidMove: isValidMove,
            fen: socket.gameState.referee.getDarkFen(socket.gameState.playerColor),
            turn: socket.gameState.playerColor,
            captured: socket.gameState.captured,
            isGameOver: false,
            san: null,
        },
        feedback: 'Illegal move.',
    };

    if (!isValidMove) {
        socket.Send(response);
        return;
    }

    if (move0x88.captured) {
        let opponentColor = Darkchess.swapColor(socket.gameState.playerColor);
        socket.gameState.captured[opponentColor][move0x88.captured]++;
    }

    socket.gameState.referee.move(move0x88);
    socket.gameState.toMove = false;

    response.content.san = Darkchess.getSAN(move0x88);
    response.content.fen = socket.gameState.referee.getDarkFen(socket.gameState.playerColor);
    response.content.turn = socket.gameState.referee.getTurn();
    response.content.captured = socket.gameState.captured;

    response.feedback = 'Opponent is thinking';

    let isGameOver = socket.gameState.referee.isGameOver();
    if (isGameOver) {
        sendQuitResponse(socket, response);
        return;
    }

    socket.Send(response, () => {

        if (isGameOver) {
            return;
        }

        Computer.registerMove(move0x88);

        let opponentMove = Computer.move();

        if (opponentMove.captured) {
            socket.gameState.captured[socket.gameState.playerColor][opponentMove.captured]++;
        }

        socket.gameState.referee.move(opponentMove);

        isGameOver = socket.gameState.referee.isGameOver();
        if (isGameOver) {
            sendQuitResponse(socket, response);
            return;
        }

        response.content.san = socket.gameState.referee.getSAN(opponentMove, socket.gameState.playerColor);
        response.content.fen = socket.gameState.referee.getDarkFen(socket.gameState.playerColor);
        response.content.turn = socket.gameState.referee.getTurn();
        response.content.captured = socket.gameState.captured;

        response.feedback = 'Your turn.';

        socket.Send(response);
        socket.gameState.toMove = true;
        if (socket.gameState.resigned) resigning(socket);

    });
}

function sendQuitResponse(socket, response) {

    let san = '1/2 - 1/2';
    let gameOverCondition = '';

    if (socket.gameState.referee.isInsufficientMaterial()) {

        gameOverCondition = 'Draw by insufficient material.';

    } else if (socket.gameState.referee.hit50MoveRule()) {

        gameOverCondition = 'Draw by 50 move rule.';

    } else {

        san = '0 - 1';
        let winner = 'Black';

        if (socket.gameState.referee.getWinner() === Darkchess.WHITE) {
            winner = 'White';
            san = '1 - 0';
        }

        gameOverCondition = winner + ' won by capture.';
    }

    response.content.fen = socket.gameState.referee.getFen();
    response.content.san = san;
    response.content.isGameOver = true;
    response.feedback = gameOverCondition;

    socket.gameState.isPlaying = false;

    socket.Send(response);
}

function onResignation(socket) {
    socket.gameState.resigned = true;
    if (socket.gameState.toMove) {
        resigning(socket);
    }
}

function resigning(socket) {

    //  NOTE(thomas): Prevent resigning multiple times.
    if (socket.gameState.isPlaying) {
        socket.gameState.isPlaying = false;

        let winner = (socket.gameState.playerColor === Darkchess.WHITE)? 'Black': 'White';

        let response = {
            type: MESSAGE.RESIGN,
            content: {
                fen: socket.gameState.referee.completeFen(),
                san: (socket.gameState.playerColor === Darkchess.WHITE)? '0 - 1': '1 - 0'
            },
            feedback: winner + ' won by resignation.'
        };

        socket.Send(response);
    }
}

/*
    #####################
    #   User Testing    #
    #####################
*/

function saveUserTest(client) {

    let header = '[Event "User Testing"]';
    let pgnData = client.game.saveToPGN();
    let fileContent = header + '\n' + pgnData;
    console.log('PGN-Data', pgnData, '\n');

    let filenameUuid = testID + '-' + fileName + '-' + UuidV1();

    let path = './userTests/backup/' + filenameUuid + '.pgn';
    saveFile(path, fileContent, () => {
        console.log('Backup has been saved');
    });

    let directory = './userTests/' + testID;
    if (!Fs.existsSync(directory)) {
        Fs.mkdir(directory, (err) => {
            if (err) {
                console.log('Failed to create directory', err);

            } else {

                let path = directory + '/' + fileName + '.pgn';
                saveFile(path, fileContent, () => {
                    console.log('File "' + fileName + '" has been saved in "' + directory + '"');
                });

            }
        });
    } else {

        let path = directory + '/' + fileName + '.pgn';
        saveFile(path, fileContent, () => {
            console.log('File "' + fileName + '" has been saved in "' + directory + '"');
        });
    }

}

function saveFile(path, content, onSuccess) {

    Fs.writeFile(path, content, (err) => {
        if (err) {
            console.log('Error writing file', err);
        } else {
            onSuccess();
        }
    });

}
