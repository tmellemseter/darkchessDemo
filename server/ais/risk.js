'use strict';

let Darkchess = require('../node_modules/darkchess/darkchess');
let Evaluation = require('./evaluation');

function Risk() {

    let darkchess = {};
    let riskFactor = 1.0;
    let playerColor = null;

    // PRIVATE FUNCTIONS:

    function noneCanAttack(n, possible, total) {

        let result = 1;

        for (let i = 0; i < n; i++) {
            result *= (total - possible - i) / (total - i);
        }

        return result;
    }

    function getLessValuedPieces(piece) {

        let result = [];

        let sortedByValue = Evaluation.getPieceTypesSortedByBaseValue();
        for (let i = 0; i < sortedByValue.length; i++) {
            if (piece === sortedByValue[i]) break;
            result.push(sortedByValue[i]);
        }

        return result;
    }

    function getDarkFileInfo(inViewInfo) {

        let filesInfo = [0, 0, 0, 0, 0, 0, 0, 0];

        let firstRank = { b: Darkchess.RANK_8, w: Darkchess.RANK_1 };
        let lastRank = { b: Darkchess.RANK_1, w: Darkchess.RANK_8 };

        for (let i = Darkchess.SQUARES.a8; i <= Darkchess.SQUARES.h1; i++) {

            let isOffTheBoard = (i & 0x88);
            if (isOffTheBoard) { i += 7; continue; }

            let rank = Darkchess.rank(i);
            if (rank === firstRank[playerColor] || rank === lastRank[playerColor]) continue;

            let file = Darkchess.file(i);

            let isDark = !inViewInfo[i];
            if (isDark) {
                filesInfo[file] += 1;
            }
        }

        return filesInfo;
    }

    function getDarkSquaresInfo(inViewInfo) {

        let info = {};
        info[Darkchess.LIGHT] = 0;
        info[Darkchess.DARK] = 0;

        for (let i = Darkchess.SQUARES.a8; i <= Darkchess.SQUARES.h1; i++) {

            let isOffTheBoard = (i & 0x88);
            if (isOffTheBoard) { i += 7; continue; }

            let isInView = inViewInfo[i];
            if (isInView) continue;

            let squareColor = Darkchess.squareColor(i);
            info[squareColor]++;
        }

        info['total'] = info[Darkchess.LIGHT] + info[Darkchess.DARK];
        return info;
    }

    function assessPieceRisk(sourceSquare, sourcePiece, inViewInfo, opponentPieceInfo) {

        let darkSquaresInfo = getDarkSquaresInfo(inViewInfo);
        let darkFileInfo = getDarkFileInfo(inViewInfo);
        let lessValuedPieces = getLessValuedPieces(sourcePiece);

        let numberOfAttackSquares = {};
        numberOfAttackSquares[Darkchess.PAWN] = 0;
        numberOfAttackSquares[Darkchess.KNIGHT] = 0;
        numberOfAttackSquares[Darkchess.BISHOP] = 0;
        numberOfAttackSquares[Darkchess.ROOK] = 0;
        numberOfAttackSquares[Darkchess.QUEEN] = 0;

        let firstRank = { b: Darkchess.RANK_8, w: Darkchess.RANK_1 };
        let lastRank = { b: Darkchess.RANK_1, w: Darkchess.RANK_8 };

        let results = {};

        // Check every less valued piece for possible attack options.
        for (let i = 0; i < lessValuedPieces.length; i++) {
            let piece = lessValuedPieces[i];

            let pieceResult = null;
            if (piece === Darkchess.PAWN) {

                 // If a pawn is in the dark and can capture, the number of
                 // dark squares on that file has to be at least one.
                let darkSquaresOnFile = 1;
                let noneAttacking = 1;

                // Check any pawn capture possibility
                for (let j = 2; j < 4; j++) {

                    let square = sourceSquare + Darkchess.PAWN_OFFSETS[playerColor][j];

                    let isOffTheBoard = (square & 0x88);
                    if (isOffTheBoard) continue;

                    // Only assessing unknown risks.
                    let attackIsVisible = inViewInfo[square];
                    if (attackIsVisible) continue;

                    // Pawns cannot be on the first or last rank
                    let rank = Darkchess.rank(square);
                    if (rank === firstRank[playerColor] || rank === lastRank[playerColor]) continue;

                    let file = Darkchess.file(square);
                    darkSquaresOnFile = darkFileInfo[file];

                    let numberOfPawns = 1;
                    let possibleAttackSquares = 1;
                    noneAttacking *= noneCanAttack(numberOfPawns, possibleAttackSquares, darkSquaresOnFile);
                }

                let atLeastOneCanAttack = 1 - noneAttacking;
                pieceResult = atLeastOneCanAttack;

            } else {

                let offsets = Darkchess.PIECE_OFFSETS[piece].length;

                for (let j = 0; j < offsets; j++) {
                    let offset = Darkchess.PIECE_OFFSETS[piece][j];
                    let square = sourceSquare;

                    while (true) {
                        square += offset;

                        let isOffTheBoard = (square & 0x88);
                        if (isOffTheBoard) break;

                        // When counting number of possible attack squares, we need to
                        // stop when seeing a blocking piece, no matter the color.
                        let squareIsVisible = inViewInfo[square];
                        if (squareIsVisible) {

                            let blockingPiece = darkchess.getPiece(square);
                            if (blockingPiece) break;

                        } else {

                            numberOfAttackSquares[piece] += 1;
                        }

                        if (piece === Darkchess.KNIGHT) break;
                    }
                }

                let numberOfPieces = 0;
                let countType = 'total';
                if (piece === Darkchess.BISHOP) {
                    countType = Darkchess.squareColor(sourceSquare);
                    numberOfPieces = opponentPieceInfo[piece][countType][Darkchess.HIDDEN];
                } else {
                    numberOfPieces = opponentPieceInfo[piece][Darkchess.HIDDEN];
                }

                let atLeastOneCanAttack = 1 - noneCanAttack(numberOfPieces, numberOfAttackSquares[piece], darkSquaresInfo[countType]);
                pieceResult = atLeastOneCanAttack;
            }

            results[piece] = pieceResult.toFixed(2) * 1;
        }

        let noneOccurr = 1;
        for (let i = 0; i < lessValuedPieces.length; i++) {
            let piece = lessValuedPieces[i];

            noneOccurr *= (1 - results[piece]);
        }

        let atLeasOneCanOccur = (1 - noneOccurr);
        return atLeasOneCanOccur
    }

    // PUBLIC FUNCTIONS:

    function setRiskFactor(factor) {
        riskFactor = factor;
    }

    function assess(color, fen, inViewInfo, opponentPieceInfo) {

        playerColor = color;

        darkchess = new Darkchess();
        darkchess.setState(fen);

        let board = darkchess.getBoard();
        let turn = darkchess.getTurn();

        let result = [];

        for (let i = Darkchess.SQUARES.a8; i <= Darkchess.SQUARES.h1; i++) {

            let isOffTheBoard = (i & 0x88);
            if (isOffTheBoard) { i += 7; continue; }

            // Only search what is visible
            let squareNotVisible = !inViewInfo[i];
            if (squareNotVisible) continue;

            // Only assess risk on own pieces
            let piece = board[i];
            if (piece == null || piece.color !== playerColor) continue;

            let risk = assessPieceRisk(i, piece.type, inViewInfo, opponentPieceInfo);

            let scaledRisk = risk * riskFactor;
            let potentialLoss = (Evaluation.PIECE_BASE_VALUES[piece.type] * scaledRisk * 0.01).toFixed(2) * 1;
            result.push(potentialLoss);
        }

        let totalRisk = 0;
        for (let i = 0; i < result.length; i++) {
            let risk = result[i];
            totalRisk += risk;
        }

        return totalRisk;
    }

    return {
        setRiskFactor: setRiskFactor,
        assess: assess
    };
}

module.exports = Risk;
