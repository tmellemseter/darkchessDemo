'use strict';

const Darkchess = require('../node_modules/darkchess/darkchess');

function Evaluation() {

    let darkchess = {};
    let pieceSquareTables = {};

    initializePieceSquareTables();

    // PRIVATE FUNCTIONS:

    function initializePieceSquareTables() {

        pieceSquareTables[Darkchess.WHITE] = {};
        pieceSquareTables[Darkchess.WHITE][Darkchess.PAWN] = convert2D8x8to1D0x88(Evaluation.PAWN_SQUARE_VALUES);
        pieceSquareTables[Darkchess.WHITE][Darkchess.KNIGHT] = convert2D8x8to1D0x88(Evaluation.KNIGHT_SQUARE_VALUES);
        pieceSquareTables[Darkchess.WHITE][Darkchess.BISHOP] = convert2D8x8to1D0x88(Evaluation.BISHOP_SQUARE_VALUES);
        pieceSquareTables[Darkchess.WHITE][Darkchess.ROOK] = convert2D8x8to1D0x88(Evaluation.ROOK_SQUARE_VALUES);
        pieceSquareTables[Darkchess.WHITE][Darkchess.QUEEN] = convert2D8x8to1D0x88(Evaluation.QUEEN_SQUARE_VALUES);
        pieceSquareTables[Darkchess.WHITE][Darkchess.KING] = convert2D8x8to1D0x88(Evaluation.KING_SQUARE_VALUES);

        mirror2DArray(Evaluation.PAWN_SQUARE_VALUES);
        mirror2DArray(Evaluation.KNIGHT_SQUARE_VALUES);
        mirror2DArray(Evaluation.BISHOP_SQUARE_VALUES);
        mirror2DArray(Evaluation.ROOK_SQUARE_VALUES);
        mirror2DArray(Evaluation.QUEEN_SQUARE_VALUES);
        mirror2DArray(Evaluation.KING_SQUARE_VALUES);

        pieceSquareTables[Darkchess.BLACK] = {};
        pieceSquareTables[Darkchess.BLACK][Darkchess.PAWN] = convert2D8x8to1D0x88(Evaluation.PAWN_SQUARE_VALUES);
        pieceSquareTables[Darkchess.BLACK][Darkchess.KNIGHT] = convert2D8x8to1D0x88(Evaluation.KNIGHT_SQUARE_VALUES);
        pieceSquareTables[Darkchess.BLACK][Darkchess.BISHOP] = convert2D8x8to1D0x88(Evaluation.BISHOP_SQUARE_VALUES);
        pieceSquareTables[Darkchess.BLACK][Darkchess.ROOK] = convert2D8x8to1D0x88(Evaluation.ROOK_SQUARE_VALUES);
        pieceSquareTables[Darkchess.BLACK][Darkchess.QUEEN] = convert2D8x8to1D0x88(Evaluation.QUEEN_SQUARE_VALUES);
        pieceSquareTables[Darkchess.BLACK][Darkchess.KING] = convert2D8x8to1D0x88(Evaluation.KING_SQUARE_VALUES);

        //  Convert back in case these tables gets referenced
        mirror2DArray(Evaluation.PAWN_SQUARE_VALUES);
        mirror2DArray(Evaluation.KNIGHT_SQUARE_VALUES);
        mirror2DArray(Evaluation.BISHOP_SQUARE_VALUES);
        mirror2DArray(Evaluation.ROOK_SQUARE_VALUES);
        mirror2DArray(Evaluation.QUEEN_SQUARE_VALUES);
        mirror2DArray(Evaluation.KING_SQUARE_VALUES);
    }

    function mirror2DArray(array) {
        for (let i = 0; i < array.length; i++) {
            array[i].reverse();
        }
        array.reverse();
    }

    function convert2D8x8to1D0x88(grid2D8x8) {

        let currentRowIndex = 0;
        let result = [];
        for (let i = Darkchess.SQUARES.a8; i <= Darkchess.SQUARES.h1; i++) {

            let isOffTheBoard = (i & 0x88);
            if (isOffTheBoard) { i += 7; continue; }

            result[i] = grid2D8x8[currentRowIndex][i % 8];

            let nextIsOfBoard = ((i + 1) & 0x88);
            if (nextIsOfBoard) currentRowIndex++;
        }

        return result;
    }

    function getAverageSquareValue(color, pieceType, inView, squareColor) {

        let squareValues0x88 = pieceSquareTables[color][pieceType];

        let squareValue = 0;
        let numberOfDarkSquares = 0;

        for (let i = Darkchess.SQUARES.a8; i <= Darkchess.SQUARES.h1; i++) {

            let isOffTheBoard = (i & 0x88);
            if (isOffTheBoard) { i += 7; continue; }

            if (inView[i]) continue;

            if (squareColor !== undefined && Darkchess.squareColor(i) !== squareColor) continue;

            numberOfDarkSquares++;
            squareValue += squareValues0x88[i];
        }

        if (numberOfDarkSquares === 0) return 0;

        let averageSquareValue = squareValue / numberOfDarkSquares;
        return averageSquareValue;
    }

    function getVisiblePiecesValue(color, pieceType, inView) {

        let squareValues0x88 = pieceSquareTables[color][pieceType];
        let board = darkchess.getBoard();

        let collectedPieceValue = 0;
        for (let i = Darkchess.SQUARES.a8; i <= Darkchess.SQUARES.h1; i++) {

            let isOffTheBoard = (i & 0x88);
            if (isOffTheBoard) { i += 7; continue; }

            // Only consider visible
            if (!inView[i]) continue;

            let piece = board[i];
            if (piece == null || piece.color !== color) continue;

            if (piece.type !== pieceType) continue;

            collectedPieceValue += (Evaluation.PIECE_BASE_VALUES[pieceType] + squareValues0x88[i]);
        }

        return collectedPieceValue;
    }

    function getMaterialDifference(opponentColor, piece, inView, pieceInfo) {

        let baseValue = Evaluation.PIECE_BASE_VALUES[piece];
        let squareValues = Evaluation.PIECE_SQUARE_TABLES[piece]

        let knownTotalPieceValueWhite = getVisiblePiecesValue(Darkchess.WHITE, piece, inView);
        let knownTotalPieceValueBlack = getVisiblePiecesValue(Darkchess.BLACK, piece, inView);

        let pieceEstimate = 0;
        if (piece === Darkchess.BISHOP) {

            let averageLightSquareValue = getAverageSquareValue(opponentColor, piece, inView, Darkchess.LIGHT);
            let lightBishopEstimate = pieceInfo[Darkchess.LIGHT][Darkchess.HIDDEN] * (baseValue + averageLightSquareValue);

            let averageDarkSquareValue = getAverageSquareValue(opponentColor, piece, inView, Darkchess.DARK);
            let darkBishopEstimate = pieceInfo[Darkchess.DARK][Darkchess.HIDDEN] * (baseValue + averageDarkSquareValue);

            pieceEstimate = lightBishopEstimate + darkBishopEstimate;

        } else {
            let averageSquareValue = getAverageSquareValue(opponentColor, piece, inView);
            pieceEstimate = pieceInfo[Darkchess.HIDDEN] * (baseValue + averageSquareValue);
        }

        if (isNaN(pieceEstimate)) console.log('Estimate is NAN!');

        if (opponentColor === Darkchess.WHITE) {
            knownTotalPieceValueWhite += pieceEstimate;
        } else {
            knownTotalPieceValueBlack += pieceEstimate;
        }

        let pieceDifference = (knownTotalPieceValueWhite - knownTotalPieceValueBlack);
        return pieceDifference;
    }

    //  PUBLIC FUNCTIONS

    function evaluate(fen, opponentColor, inView, pieceInfo) {

        darkchess = new Darkchess();
        darkchess.setState(fen);

        let kingDifference = getMaterialDifference(opponentColor, Darkchess.KING, inView, pieceInfo[Darkchess.KING]);
        if (isNaN(kingDifference)) console.log('KING DIFF NAN!');

        let pawnDifference = getMaterialDifference(opponentColor, Darkchess.PAWN, inView, pieceInfo[Darkchess.PAWN]);
        if (isNaN(pawnDifference)) console.log('PAWN DIFF NAN!');

        let rookDifference = getMaterialDifference(opponentColor, Darkchess.ROOK, inView, pieceInfo[Darkchess.ROOK]);
        if (isNaN(rookDifference)) console.log('ROOK DIFF NAN!');

        let queenDifference = getMaterialDifference(opponentColor, Darkchess.QUEEN, inView, pieceInfo[Darkchess.QUEEN]);
        if (isNaN(queenDifference)) console.log('QUEEN DIFF NAN!');

        let knightDifference = getMaterialDifference(opponentColor, Darkchess.KNIGHT, inView, pieceInfo[Darkchess.KNIGHT]);
        if (isNaN(knightDifference)) console.log('KNIGHT DIFF NAN!');

        let bishopDifference = getMaterialDifference(opponentColor, Darkchess.BISHOP, inView, pieceInfo[Darkchess.BISHOP]);
        if (isNaN(bishopDifference)) console.log('BISHOP DIFF NAN!');

        let material = 0.01 * (kingDifference + queenDifference + rookDifference +
                              bishopDifference + knightDifference + pawnDifference);

        let who2move = (darkchess.getTurn() === Darkchess.WHITE)? 1: -1;
        return material * who2move;
    }

    return {
        evaluate: evaluate
    };
}

Evaluation.PIECE_BASE_VALUES = {
    p: 100,
    n: 320,
    b: 330,
    r: 500,
    q: 900,
    k: 20000
};

Evaluation.PAWN_SQUARE_VALUES = [
    [   0,   0,   0,   0,   0,   0,   0,   0],
    [  50,  50,  50,  50,  50,  50,  50,  50],
    [  10,  10,  20,  30,  30,  20,  10,  10],
    [   5,   5,  10,  25,  25,  10,   5,   5],
    [   0,   0,   0,  20,  20,   0,   0,   0],
    [   5,  -5, -10,   0,   0, -10,  -5,   5],
    [   5,  10,  10, -20, -20,  10,  10,   5],
    [   0,   0,   0,   0,   0,   0,   0,   0],
];

Evaluation.KNIGHT_SQUARE_VALUES = [
    [ -50, -40, -30, -30, -30, -30, -40, -50],
    [ -40, -20,   0,   0,   0,   0, -20, -40],
    [ -30,   0,  10,  15,  15,  10,   0, -30],
    [ -30,   5,  15,  20,  20,  15,   5, -30],
    [ -30,   0,  15,  20,  20,  15,   0, -30],
    [ -30,   5,  10,  15,  15,  10,   5, -30],
    [ -40, -20,   0,   5,   5,   0, -20, -40],
    [ -50, -40, -30, -30, -30, -30, -40, -50],
];

Evaluation.BISHOP_SQUARE_VALUES = [
    [ -20, -10, -10, -10, -10, -10, -10, -20],
    [ -10,   0,   0,   0,   0,   0,   0, -10],
    [ -10,   0,   5,  10,  10,   5,   0, -10],
    [ -10,   5,   5,  10,  10,   5,   5, -10],
    [ -10,   0,  10,  10,  10,  10,   0, -10],
    [ -10,  10,  10,  10,  10,  10,  10, -10],
    [ -10,   5,   0,   0,   0,   0,   5, -10],
    [ -20, -10, -10, -10, -10, -10, -10, -20],
];

Evaluation.ROOK_SQUARE_VALUES = [
    [   0,   0,   0,   0,   0,   0,   0,   0],
    [   5,  10,  10,  10,  10,  10,  10,   5],
    [  -5,   0,   0,   0,   0,   0,   0,  -5],
    [  -5,   0,   0,   0,   0,   0,   0,  -5],
    [  -5,   0,   0,   0,   0,   0,   0,  -5],
    [  -5,   0,   0,   0,   0,   0,   0,  -5],
    [  -5,   0,   0,   0,   0,   0,   5,  -5],
    [   0,   0,   0,   5,   5,   0,   0,   0],
];

Evaluation.QUEEN_SQUARE_VALUES = [
    [ -20, -10, -10,  -5,  -5, -10, -10, -20],
    [ -10,   0,   0,   0,   0,   0,   0, -10],
    [ -10,   0,   5,   5,   5,   5,   0, -10],
    [  -5,   0,   5,   5,   5,   5,   0,  -5],
    [   0,   0,   5,   5,   5,   5,   0,  -5],
    [ -10,   5,   5,   5,   5,   5,   0, -10],
    [ -10,   0,   5,   0,   0,   0,   0, -10],
    [ -20, -10, -10,  -5,  -5, -10, -10, -20],
];

Evaluation.KING_SQUARE_VALUES = [
    [ -30, -40, -40, -50, -50, -40, -40, -50],
    [ -30, -40, -40, -50, -50, -40, -40, -30],
    [ -30, -40, -40, -50, -50, -40, -40, -30],
    [ -30, -40, -40, -50, -50, -40, -40, -30],
    [ -20, -30, -30, -40, -40, -30, -30, -20],
    [ -10, -20, -20, -20, -20, -20, -20, -10],
    [  20,  20,   0,   0,   0,   0,  20,  20],
    [  20,  30,  10,   0,   0,  10,  30,  20],
];

Evaluation.PIECE_SQUARE_TABLES = {
    p: Evaluation.PAWN_SQUARE_VALUES,
    n: Evaluation.KNIGHT_SQUARE_VALUES,
    b: Evaluation.BISHOP_SQUARE_VALUES,
    r: Evaluation.ROOK_SQUARE_VALUES,
    q: Evaluation.QUEEN_SQUARE_VALUES,
    k: Evaluation.KING_SQUARE_VALUES
};

Evaluation.getPieceTypesSortedByBaseValue = function() {
    let sortedByValue = Object.keys(Evaluation.PIECE_BASE_VALUES).sort(function(a, b) {
        return Evaluation.PIECE_BASE_VALUES[a] - Evaluation.PIECE_BASE_VALUES[b];
    });

    return sortedByValue;
}

module.exports = Evaluation;
