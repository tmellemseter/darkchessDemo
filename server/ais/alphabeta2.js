'use strict'

var Utils = require('../utils');
const Darkchess = require('../node_modules/darkchess/darkchess');

function AlphaBeta() {

    let self = this;

    self.log = {
        search: false,
        result: false
    };

    self.assessRisk = false;

    self.depth = null;
    self.lines = [];

    self.principalVariations = [];

    self.tempData = [];
    self.tempRisk = [];

    self.calls = {
        assessRisk: null,
        evaluate: null,
        getItems: null,
        getQuiesceItems: null,

        beforeRecursiveQuiesceCall: null,
        afterRecursiveQuiesceCall: null,

        returnConditionCall: null,

        beforeRecursiveCall: null,
        afterRecursiveCall: null,

        beforeNoneItemsRecursiveCall: null,
        afterNoneItemsRecursiveCall: null,
    };

    // PUBLIC FUNCTIONS

    function enableSearchLog()      {   self.log.search = true;         }
    function enableResultLog()      {   self.log.result = true;         }
    function setDepth(depth)        {   self.depth = depth;             }
    function setAssessRisk(assess)  {   self.calls.assessRisk = assess; }
    function setEvaluate(evaluate)  {   self.calls.evaluate = evaluate; }
    function setGetItems(call)      {   self.calls.getItems = call;     }
    function enableRiskAssessment() {   self.assessRisk = true;         }

    function setRetrieveRiskCall(call)  {   self.calls.retrieveRisk = call;     }
    function setGetQuiesceItems(call)   {   self.calls.getQuiesceItems = call;  }

    function setBeforeRecursiveQuiesceCall(call) {  self.calls.beforeRecursiveQuiesceCall = call;   }
    function setAfterRecursiveQuiesceCall(call)  {  self.calls.afterRecursiveQuiesceCall = call;    }

    function setReturnConditionCall(call)   {   self.calls.returnConditionCall = call;  }

    function setBeforeRecursiveCall(call)   {   self.calls.beforeRecursiveCall = call;  }
    function setAfterRecursiveCall(call)    {   self.calls.afterRecursiveCall = call;    }

    function setBeforeNoneItemsRecursiveCall(call)  {   self.calls.beforeNoneItemsRecursiveCall = call; }
    function setAfterNoneItemsRecursiveCall(call)   {   self.calls.afterNoneItemsRecursiveCall = call;  }

    function search(startMaximize) {

        // Reset values.
        self.lines = [];
        self.principalVariations = [];

        Utils.assert(self.calls.evaluate);
        Utils.assert(self.calls.getItems);
        Utils.assert(self.calls.getQuiesceItems);
        Utils.assert(self.calls.returnConditionCall);
        Utils.assert(self.calls.beforeRecursiveCall);
        Utils.assert(self.calls.afterRecursiveCall);
        Utils.assert(self.calls.beforeNoneItemsRecursiveCall);
        Utils.assert(self.calls.afterNoneItemsRecursiveCall);

        Utils.assert(self.calls.beforeRecursiveQuiesceCall);
        Utils.assert(self.calls.afterRecursiveQuiesceCall);

        let principalLine = [];
        let score = abNegamax(-Infinity, Infinity, 0, principalLine);

        if (self.log.result) {
            console.log('\n\tRETURNED SCORE', score.toFixed(2));

            console.log('\n\tALPHA BETA\t' + ((startMaximize)? 'MAX': 'MIN'));
            console.log('\n\tNr.\tScore\tRisk\tTotal\tMoves');

            for (let i = 0; i < self.lines.length; i++) {
                let logLine = self.lines[i];

                let simpleNotation = '';

                for (let j = 0; j < logLine.items.length; j++) {
                    let item = logLine.items[j];
                    let simple = item;
                    if (typeof item === 'object') {
                        simple = Darkchess.getSanSimple(item);
                    }

                    simpleNotation += simple + ' ';
                }

                let score = logLine.score.toFixed(2);
                let risk = logLine.risk.toFixed(2);
                let included = (score - risk).toFixed(2);

                console.log('\t' + i + '\t' + -score + '\t' +  risk + '\t' + -included + '\t' + simpleNotation);
            }

            console.log('\n\tPV:', Darkchess.logMoves(principalLine));
        }

        /*
        for (let i = 0; i < self.principalVariations.length; i++) {
            let variation = self.principalVariations[i];
            console.log('\t' + Darkchess.getSanSimple(variation.move) + ' --> ' + variation.score.toFixed(2));
        }*/

        let movesToConsider = self.principalVariations.sort((a, b) => {
            return a.score - b.score;
        });

        // Remove baddest moves
        if (movesToConsider.length > 2) {
            movesToConsider.splice(0, 1);
        }

        if (movesToConsider.length === 0) {
            console.log(principalLine, score);
            console.log('s l', self.lines);

        }

        return movesToConsider;
    }

    //  PRIVATE FUNCTIONS

    function negaMax(depth, pline) {

        let line1 = [];

        let gameFinished = self.calls.returnConditionCall();
        if (depth == self.depth || gameFinished) {
            return self.calls.evaluate();
        }

        let items = self.calls.getItems();

        if (items.length === 0) {
            self.calls.beforeNoneItemsRecursiveCall();
            let score = -negaMax(depth + 1, line1);
            self.calls.afterNoneItemsRecursiveCall();

            // Empty the array without losing the referrence
            pline.splice(0);
            for (let i = 0; i < line1.length; i++) {
                pline[i] = line1[i];
            }

            if (score > max) {
                max = score;
            }

            return max;
        }

        let max = -Infinity;
        for (let i = 0; i < items.length; i++) {
            let item = items[i];

            self.calls.beforeRecursiveCall(item);
            self.tempData.push(item);

            let score = -negaMax(depth + 1, line1);

            self.tempData.pop();
            self.calls.afterRecursiveCall(item);

            if (score > max) {
                max = score;

                let noOpponentMoves = (pline.length > (line1.length + 1));
                if (noOpponentMoves) {
                    pline.pop();
                }

                pline[0] = item;
                for (let i = 1; i <= line1.length; i++) {
                    pline[i] = line1[i - 1];
                }

                if (depth === 0) {
                    self.principalVariations.push({
                        move: pline[0],
                        score: alpha
                    });
                }
            }
        }

        return max;
    }

    function abNegamax(alpha, beta, depth, pline) {

        let line1 = [];
        let gameFinished = self.calls.returnConditionCall();

        if (depth == self.depth || gameFinished) {

            /*
            if (gameFinished) {
                return self.calls.evaluate();
            }*/

            self.tempData.push('  ->');

            let originalScore = quiesce(alpha, beta, depth, pline);

            self.tempData.pop();

            let risk = 0.0;
            let score = originalScore;

            if (self.assessRisk) {
                risk = self.calls.assessRisk();
                score = (originalScore - risk);
            }

            if (self.lines.length > 0) {
                self.lines[self.lines.length - 1].score = originalScore;
                self.lines[self.lines.length - 1].risk = risk;
            }

            /*
            self.lines.push({
                items: Utils.copy(self.tempData),
                score: originalScore,
                risk: risk
            });*/

            return score;
        }

        let items = self.calls.getItems();

        if (items.length === 0) {
            self.calls.beforeNoneItemsRecursiveCall();
            let score = -abNegamax(-beta, -alpha, depth + 1, line1);
            self.calls.afterNoneItemsRecursiveCall();

            // Empty the array without losing the referrence
            pline.splice(0);
            for (let i = 0; i < line1.length; i++) {
                pline[i] = line1[i];
            }

            if (score >= beta) {
                return beta;
            }

            if (score > alpha) {
                alpha = score;
            }

            return alpha;
        }

        for (let i = 0; i < items.length; i++) {
            let item = items[i];

            self.calls.beforeRecursiveCall(item);
            self.tempData.push(item);

            let score = -abNegamax(-beta, -alpha, depth + 1, line1);

            self.tempData.pop();
            self.calls.afterRecursiveCall(item);

            //console.log(Darkchess.getSanSimple(item), score);

            //  Fail hard beta-cutoff
            if (score >= beta) {
                return beta;
            }

            //  Alpha acts like max in MiniMax
            if (score > alpha) {
                //console.log('update alpha');
                alpha = score;
                //self.line[depth] = item;

                let noOpponentMoves = (pline.length > (line1.length + 1));
                if (noOpponentMoves) {
                    pline.pop();
                }

                pline[0] = item;
                for (let i = 1; i <= line1.length; i++) {
                    pline[i] = line1[i - 1];
                }

                if (depth === 0) {
                    self.principalVariations.push({
                        move: pline[0],
                        score: alpha
                    });
                }
            }
        }

        return alpha;
    }

    function alphabeta(depth, alpha, beta, toMaximize) {

        if (depth === 0 || self.calls.returnConditionCall()) {

            // let score1 = self.calls.evaluate();
            // console.log('\n\tSCORE1', score1.toFixed(2));

            let score = quiesce(alpha, beta);
            console.log('\tQUI SCORE', score.toFixed(2));

            //  Basically the side that is searching, if
            //  depth is an even number it is the same as toMaximize
            let firstToMaximizeFlag = self.tempRisk[0].toMaximize;

            let ownRisks = self.tempRisk.filter((risk) => {
                return (risk.toMaximize === firstToMaximizeFlag);
            });

            let totalBestCase = 1;
            ownRisks.forEach((item) => {
                totalBestCase *= (1 - item.risk);
            });

            // The risk of at least one of the moves (depth) occure
            let minimumRisk = 1 - totalBestCase;

            self.lines.push({
                items: Utils.copy(self.tempData),
                score: score * minimumRisk
            });

            return score;
        }

        if (toMaximize) {

            let max = -Infinity;
            let items = self.calls.getItems(toMaximize);

            if (items.length === 0) {
                self.calls.beforeNoneItemsRecursiveCall(toMaximize);
                let score = alphabeta(depth - 1, alpha, beta, !toMaximize);
                self.calls.afterNoneItemsRecursiveCall(toMaximize);
                return score;
            }

            for (let i = 0; i < items.length; i++) {
                let item = items[i];

                let risk = self.calls.beforeRecursiveCall(toMaximize, item);
                self.tempData.push(item);
                self.tempRisk.push({
                    toMaximize: toMaximize,
                    risk: risk
                });

                let score = alphabeta(depth - 1, alpha, beta, !toMaximize);

                self.tempData.pop();
                self.tempRisk.pop();
                self.calls.afterRecursiveCall(toMaximize, item);

                if (score > max) {
                    max = score;
                    self.line[depth] = item;
                }

                alpha = Math.max(alpha, max);
                if (beta <= alpha) break;
            }

            return max;
        } else {

            let min = Infinity;
            let items = self.calls.getItems(toMaximize);

            if (items.length === 0) {
                self.calls.beforeNoneItemsRecursiveCall(toMaximize);
                let score = alphabeta(depth - 1, alpha, beta, !toMaximize);
                self.calls.afterNoneItemsRecursiveCall(toMaximize);
                return score;
            }

            for (let i = 0; i < items.length; i++) {
                let item = items[i];

                let risk = self.calls.beforeRecursiveCall(toMaximize, item);
                self.tempData.push(item);
                self.tempRisk.push({
                    toMaximize: toMaximize,
                    risk: risk
                });

                let score = alphabeta(depth - 1, alpha, beta, !toMaximize);

                self.tempData.pop();
                self.tempRisk.pop();
                self.calls.afterRecursiveCall(toMaximize, item);

                if (score < min) {
                    min = score;
                    self.line[depth] = item;
                }

                beta = Math.min(beta, min);
                if (beta <= alpha) break;
            }

            return min;
        }
    }

    function quiesce(alpha, beta, depth, pline) {

        //  NOTE(thomas):
        //  This function could go for a long time, depending on the number
        //  of capturing moves :|

        let line2 = [];

        //  StandPat: Establish a lower bound, this is theoretically sound
        //  because we can usually assume that there is at least one move
        //  that can either match or beat the lower bound.
        //  Based on Null Move Observation.

        let standPat = self.calls.evaluate();

        if (standPat >= beta) {
            return beta;        // fail hard
            //return standPat;    // fail soft
        }

        self.lines.push({
            items: Utils.copy(self.tempData),
            score: standPat,
            risk: 0.0,
        });

        if (alpha < standPat) {
            alpha = standPat;
            //let m = self.tempData[self.tempData.length - 1];
            //self.line[depth] = m;
        }

        let quiesceItems = self.calls.getQuiesceItems();

        for (let i = 0; i < quiesceItems.length; i++) {
            let item = quiesceItems[i];

            self.calls.beforeRecursiveQuiesceCall(item);
            self.tempData.push(item);

            //self.lines.pop();
            let score = -quiesce(-beta, -alpha, depth + 1, line2);

            self.tempData.pop();
            self.calls.afterRecursiveQuiesceCall(item);

            if (score >= beta) {
                return beta;
            }

            if (score > alpha) {
                alpha = score;
                //let m = self.tempData[self.tempData.length - 1];
                //self.line[depth] = m;

                pline.push(item);

                pline[0] = item;
                for (let i = 1; i <= line2.length; i++) {
                    pline[i] = line2[i - 1];
                }
            }
        }

        return alpha;
    }

    return {
        search: search,

        enableRiskAssessment: enableRiskAssessment,
        enableResultLog: enableResultLog,
        enableSearchLog: enableSearchLog,
        setDepth: setDepth,

        setAssessRisk: setAssessRisk,
        setEvaluate: setEvaluate,
        setGetItems: setGetItems,

        setGetQuiesceItems: setGetQuiesceItems,
        setBeforeRecursiveQuiesceCall: setBeforeRecursiveQuiesceCall,
        setAfterRecursiveQuiesceCall: setAfterRecursiveQuiesceCall,

        setReturnConditionCall: setReturnConditionCall,
        setBeforeRecursiveCall: setBeforeRecursiveCall,
        setAfterRecursiveCall: setAfterRecursiveCall,
        setBeforeNoneItemsRecursiveCall: setBeforeNoneItemsRecursiveCall,
        setAfterNoneItemsRecursiveCall: setAfterNoneItemsRecursiveCall,
    }

}

module.exports = AlphaBeta;
