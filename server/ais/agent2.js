'use strict'

let Utils = require('../utils');
let AlphaBeta = require('./alphabeta2');
let Darkchess = require('../../node_modules/darkchess/darkchess');
let Evaluation = require('./evaluation');
let Risk = require('./risk');

function Agent(id, color) {

    let self = this;

    self.id = id;
    self.color = null;
    self.eloRating = 500;

    self.alphaBeta = null;
    self.darkchess = null;
    self.randomGenerator = null;
    self.evaluate = null;

    self.opponentPieceInfo = {};
    self.opponentPieceInfoError = false;

    self.riskFactor = 1.0;
    self.onlyBest = false;
    self.sortMoves = false;

    self.log = {
        search: false
    };

    initializeAgent();

    // Functions
    function initializeAgent() {

        self.randomGenerator = Math;
        self.alphaBeta = new AlphaBeta();

        let evaluation = new Evaluation();
        self.evaluate = evaluation.evaluate;

        resetOpponentPieceInfo();
    }

    function setRiskFactor(factor) {
        self.riskFactor = factor;
    }

    function resetOpponentPieceInfo() {
        self.opponentPieceInfoError = false;

        self.opponentPieceInfo = {};
        self.opponentPieceInfo[Darkchess.KING] = {}
        self.opponentPieceInfo[Darkchess.KING][Darkchess.VISIBLE] = 0;
        self.opponentPieceInfo[Darkchess.KING][Darkchess.HIDDEN] = 1;

        self.opponentPieceInfo[Darkchess.QUEEN] = {}
        self.opponentPieceInfo[Darkchess.QUEEN][Darkchess.VISIBLE] = 0;
        self.opponentPieceInfo[Darkchess.QUEEN][Darkchess.HIDDEN] = 1;

        self.opponentPieceInfo[Darkchess.ROOK] = {}
        self.opponentPieceInfo[Darkchess.ROOK][Darkchess.VISIBLE] = 0;
        self.opponentPieceInfo[Darkchess.ROOK][Darkchess.HIDDEN] = 2;

        self.opponentPieceInfo[Darkchess.BISHOP] = {}
        self.opponentPieceInfo[Darkchess.BISHOP][Darkchess.LIGHT] = {};
        self.opponentPieceInfo[Darkchess.BISHOP][Darkchess.LIGHT][Darkchess.VISIBLE] = 0;
        self.opponentPieceInfo[Darkchess.BISHOP][Darkchess.LIGHT][Darkchess.HIDDEN] = 1;

        self.opponentPieceInfo[Darkchess.BISHOP][Darkchess.DARK] = {};
        self.opponentPieceInfo[Darkchess.BISHOP][Darkchess.DARK][Darkchess.VISIBLE] = 0;
        self.opponentPieceInfo[Darkchess.BISHOP][Darkchess.DARK][Darkchess.HIDDEN] = 1;

        self.opponentPieceInfo[Darkchess.KNIGHT] = {}
        self.opponentPieceInfo[Darkchess.KNIGHT][Darkchess.VISIBLE] = 0;
        self.opponentPieceInfo[Darkchess.KNIGHT][Darkchess.HIDDEN] = 2;

        self.opponentPieceInfo[Darkchess.PAWN] = {};
        self.opponentPieceInfo[Darkchess.PAWN][Darkchess.VISIBLE] = 0;
        self.opponentPieceInfo[Darkchess.PAWN][Darkchess.HIDDEN] = 8;
    }

    function updatePromotion(move, toRemove) {

        //  NOTE(thomas):
        //  The agents opponent can only change its material by promoting.
        if (move.promotion === Darkchess.BISHOP) {

            let squareColor = Darkchess.squareColor(move.to);
            if (toRemove) {
                self.opponentPieceInfo[Darkchess.PAWN][Darkchess.VISIBLE]--;
                self.opponentPieceInfo[move.promotion][squareColor][Darkchess.VISIBLE]++;
            } else {
                self.opponentPieceInfo[Darkchess.PAWN][Darkchess.VISIBLE]++;
                self.opponentPieceInfo[move.promotion][squareColor][Darkchess.VISIBLE]--;
            }

        } else {
            if (toRemove) {
                self.opponentPieceInfo[Darkchess.PAWN][Darkchess.VISIBLE]--;
                self.opponentPieceInfo[move.promotion][Darkchess.VISIBLE]++;
            } else {
                self.opponentPieceInfo[Darkchess.PAWN][Darkchess.VISIBLE]++;
                self.opponentPieceInfo[move.promotion][Darkchess.VISIBLE]--;
            }
        }
    }

    function updateOpponentPieceInfo(move, knewInViewInfo, knowInViewInfo) {

        let capturedPiece = move.captured;
        if (typeof capturedPiece !== 'undefined') {

            let squareColor = Darkchess.squareColor(move.to);
            let opponentPieceInfo = (capturedPiece === Darkchess.BISHOP)?
                self.opponentPieceInfo[capturedPiece][squareColor]:
                self.opponentPieceInfo[capturedPiece];

            opponentPieceInfo[Darkchess.VISIBLE]--;

            /*
            if (capturedPiece === Darkchess.BISHOP) {

                let squareColor = Darkchess.squareColor(move.to);
                self.opponentPieceInfo[capturedPiece][squareColor][Darkchess.VISIBLE]--;

            } else {
                self.opponentPieceInfo[capturedPiece][Darkchess.VISIBLE]--;
            }*/
        }

        let board = self.darkchess.getBoard();
        for (let i = Darkchess.SQUARES.a8; i <= Darkchess.SQUARES.h1; i++) {

            let isOffTheBoard = (i & 0x88);
            if (isOffTheBoard) { i += 7; continue; }

            let knew = knewInViewInfo[i];
            let know = knowInViewInfo[i];

            // Only search within visible space
            if (!knew && !know) continue;

            // Check opponent pieces only
            let piece = board[i];
            if (piece == null || piece.color === self.color) continue;

            let squareColor = Darkchess.squareColor(move.to);
            let opponentPieceInfo = (piece.type === Darkchess.BISHOP)?
                self.opponentPieceInfo[piece.type][squareColor]:
                self.opponentPieceInfo[piece.type];

            // Change piece from visible to hidden
            if (knew && !know) {

                opponentPieceInfo[Darkchess.VISIBLE]--;
                opponentPieceInfo[Darkchess.HIDDEN]++;
                /*
                if (piece.type === Darkchess.BISHOP) {

                    let squareColor = Darkchess.squareColor(i);
                    self.opponentPieceInfo[piece.type][squareColor][Darkchess.VISIBLE]--;
                    self.opponentPieceInfo[piece.type][squareColor][Darkchess.HIDDEN]++;

                } else {

                    self.opponentPieceInfo[piece.type][Darkchess.VISIBLE]--;
                    self.opponentPieceInfo[piece.type][Darkchess.HIDDEN]++;
                }*/
            }

            // Change piece from hidden to visible
            if (!knew && know) {

                opponentPieceInfo[Darkchess.VISIBLE]++;

                if (opponentPieceInfo[Darkchess.HIDDEN] > 0) {
                    opponentPieceInfo[Darkchess.HIDDEN]--;
                } else {

                    //  There has been a pawn promotion!
                    if (self.opponentPieceInfo[Darkchess.PAWN][Darkchess.HIDDEN] > 0) {
                        self.opponentPieceInfo[Darkchess.PAWN][Darkchess.HIDDEN]--;
                    } else {
                        console.log('OWN MOVE: Negative PAWN??');
                        self.opponentPieceInfoError = true;
                    }
                }

                /*
                if (piece.type === Darkchess.BISHOP) {

                    let squareColor = Darkchess.squareColor(i);
                    self.opponentPieceInfo[piece.type][squareColor][Darkchess.VISIBLE]++;
                    self.opponentPieceInfo[piece.type][squareColor][Darkchess.HIDDEN]--;

                    if (self.opponentPieceInfo[piece.type][squareColor][Darkchess.HIDDEN] < 0) {
                        self.opponentPieceInfo[piece.type][squareColor][Darkchess.HIDDEN] = 0;
                        self.opponentPieceInfo[Darkchess.PAWN][Darkchess.HIDDEN]--;

                        if (self.opponentPieceInfo[Darkchess.PAWN][Darkchess.HIDDEN] < 0) {
                            console.log(' -1 PAWNS! MOVE BISHOP');
                            self.opponentPieceInfoError = true;
                        }
                    }

                } else {

                    self.opponentPieceInfo[piece.type][Darkchess.VISIBLE]++;

                    if (piece.type === Darkchess.PAWN) continue;

                    // An unobserved pawn promotion has happened! We know that know!
                    if (self.opponentPieceInfo[piece.type][Darkchess.HIDDEN] < 0) {
                        self.opponentPieceInfo[piece.type][Darkchess.HIDDEN] = 0;
                        self.opponentPieceInfo[Darkchess.PAWN][Darkchess.HIDDEN]--;

                        if (self.opponentPieceInfo[Darkchess.PAWN][Darkchess.HIDDEN] < 0) {
                            console.log(' -1 PAWNS! MOVE ' + piece.type);
                            self.opponentPieceInfoError = true;
                        }
                    }
                }*/
            }
        }

        assureCorrectOpponentPieceInfo('After own move ' + Darkchess.getSAN(move));
    }

    function updateOpponentPieceInfoRegister(move, knewInViewInfo, knowInViewInfo) {

        let board = self.darkchess.getBoard();
        for (let i = Darkchess.SQUARES.a8; i <= Darkchess.SQUARES.h1; i++) {

            let isOffTheBoard = (i & 0x88);
            if (isOffTheBoard) { i += 7; continue; }

            let knew = knewInViewInfo[i];
            let know = knowInViewInfo[i];

            // Only search within visible space
            if (!knew && !know) continue;

            // Check opponent pieces only
            let piece = board[i];
            if (piece == null || piece.color === self.color) continue;

            let squareColor = Darkchess.squareColor(move.to);
            let opponentPieceInfo = (piece.type === Darkchess.BISHOP)?
                self.opponentPieceInfo[piece.type][squareColor]:
                self.opponentPieceInfo[piece.type];

            if (knew && !know) {
                opponentPieceInfo[Darkchess.VISIBLE]--;
                opponentPieceInfo[Darkchess.HIDDEN]++;
            }

            if (!knew && know) {
                opponentPieceInfo[Darkchess.VISIBLE]++;

                if (opponentPieceInfo[Darkchess.HIDDEN] > 0) {
                    opponentPieceInfo[Darkchess.HIDDEN]--;
                } else {
                    //  There has been a pawn promotion!
                    if (self.opponentPieceInfo[Darkchess.PAWN][Darkchess.HIDDEN] > 0) {
                        self.opponentPieceInfo[Darkchess.PAWN][Darkchess.HIDDEN]--;
                    } else {
                        console.log('REGISTER MOVE: Negative PAWN??');
                        self.opponentPieceInfoError = true;
                    }
                }
            }
        }
    }

    function updateOpponentPieceInfoForSearch(move, toRemove) {

        if (move.color !== self.color) {

            let isPromotion = (move.flags & Darkchess.BITS.PROMOTION);
            if (isPromotion) {
                //console.log('Known promotion (search)');
                //logOpponentPieceInfo('Before Update (promotion) ' + move.promotion);
                updatePromotion(move, toRemove);
                //logOpponentPieceInfo('After Update (promotion) ' + move.promotion);
                assureCorrectOpponentPieceInfo('Searching promotion');
            }

            return;
        }

        //console.log('Move ' + self.color + ' ', Darkchess.getSAN(move));
        //console.log('Captured', move.captured);

        let capturedPiece = move.captured;
        if (typeof capturedPiece !== 'undefined') {

            if (capturedPiece === Darkchess.BISHOP) {

                let squareColor = Darkchess.squareColor(move.to);
                if (toRemove) {
                    self.opponentPieceInfo[capturedPiece][squareColor][Darkchess.VISIBLE]--;
                } else {
                    self.opponentPieceInfo[capturedPiece][squareColor][Darkchess.VISIBLE]++;
                }

            } else {
                if (toRemove) {
                    self.opponentPieceInfo[capturedPiece][Darkchess.VISIBLE]--;

                } else {
                    self.opponentPieceInfo[capturedPiece][Darkchess.VISIBLE]++;
                }
            }
        }

        assureCorrectOpponentPieceInfo('Searching');
    }

    function setState(state) {
        self.darkchess = new Darkchess();
        self.darkchess.setState(state);
    }

    function setRandomGenerator(generator) {
        Utils.assert(typeof generator.random === 'function', 'Random generator has no "random" function');
        self.randomGenerator = generator;
    }

    function setRating(rating) {
        self.eloRating = rating;
    }

    function addRating(points) {
        self.eloRating += points;
    }

    function getRating() {
        return self.eloRating;
    }

    function setColor(color) {
        self.color = color;
    }

    function getPlayerId() {
        return self.id;
    }

    function getPlayerColor() {
        return self.color;
    }

    function getInView() {

        let agentsTurn = self.color === self.darkchess.getTurn();
        if (agentsTurn) {
            return self.darkchess.getInView();
        }

        let inViewInfo = self.darkchess.getInView();

        return inViewInfo;
    }

    function log() {
        self.darkchess.logBoard(self.color);
    }

    function enableRisk() {
        self.alphaBeta.enableRiskAssessment();
    }

    function enableSearchLog() {
        self.log.search = true;
    }

    function enableSearchResultLog() {
        self.alphaBeta.enableResultLog();
    }

    function setSearchDepth(depth) {
        self.alphaBeta.setDepth(depth);
    }

    function onlyBestmoves() {
        self.onlyBest = true;
    }

    function sortMoves() {
        self.sortMoves = true;
    }

    function getVisiblePieces(inViewInfo) {

        let board = self.darkchess.getBoard();

        let piecesInView = {};
        piecesInView[Darkchess.KING] = 0;
        piecesInView[Darkchess.QUEEN] = 0;
        piecesInView[Darkchess.ROOK] = 0;
        piecesInView[Darkchess.BISHOP] = {};
        piecesInView[Darkchess.BISHOP][Darkchess.LIGHT] = 0;
        piecesInView[Darkchess.BISHOP][Darkchess.DARK] = 0;
        piecesInView[Darkchess.KNIGHT] = 0;
        piecesInView[Darkchess.PAWN] = 0;

        for (let i = Darkchess.SQUARES.a8; i <= Darkchess.SQUARES.h1; i++) {

            let isOffTheBoard = (i & 0x88);
            if (isOffTheBoard) { i += 7; continue; }

            let isInDark = !inViewInfo[i];
            if (isInDark) continue;

            // Check opponentpieces only
            let piece = board[i];
            if (piece == null || piece.color === self.color) continue;

            if (piece.type === Darkchess.BISHOP) {
                let squareColor = Darkchess.squareColor(i);
                piecesInView[piece.type][squareColor]++;
            } else {
                piecesInView[piece.type]++;
            }
        }

        return piecesInView;
    }

    function move() {

        let inViewInfo = self.darkchess.getInView(self.color);
        let opponentColor = Darkchess.swapColor(self.color);

        let risk = new Risk();

        self.alphaBeta.setAssessRisk(() => {
            let fen = self.darkchess.getFen();
            return risk.assess(self.color, fen, inViewInfo, self.opponentPieceInfo);
        });

        self.alphaBeta.setEvaluate(() => {
            let fen = self.darkchess.getFen();
            return self.evaluate(fen, opponentColor, inViewInfo, self.opponentPieceInfo);
        });

        self.alphaBeta.setReturnConditionCall(self.darkchess.isGameOver);

        //  NOTE(thomas):
        //  This assures that the moves is only within the observable
        //  space.
        let moveOptions = {
            inViewInfo: inViewInfo
        };

        self.alphaBeta.setGetItems(() => {
            let moves = self.darkchess.getMoves(moveOptions);

            if (self.sortMoves) {

                moves.sort((a,b) => {

                    let aisKsideCastling = (a.flags & Darkchess.BITS.KSIDE_CASTLE);
                    let aisQSideCastling = (a.flags & Darkchess.BITS.QSIDE_CASTLE);
                    let aisCastling = (aisKsideCastling || aisQSideCastling);
                    let aisCapture = (a.flags & Darkchess.BITS.CAPTURE);

                    let bisKsideCastling = (b.flags & Darkchess.BITS.KSIDE_CASTLE);
                    let bisQSideCastling = (b.flags & Darkchess.BITS.QSIDE_CASTLE);
                    let bisCastling = (bisKsideCastling || bisQSideCastling);
                    let bisCapture = (b.flags & Darkchess.BITS.CAPTURE);

                    if (aisCastling && !bisCastling) return -1;
                    if (!aisCastling && bisCastling) return 1;

                    if (aisCapture && !bisCapture) return -1;
                    if (!aisCapture && bisCapture) return 1;

                    return 0;
                });
            }

            return moves;
        });

        let quiesceMoveOptions = {
            inViewInfo: inViewInfo,
            captureOnly: true,
        };

        self.alphaBeta.setGetQuiesceItems(() => {
            let moves = self.darkchess.getMoves(quiesceMoveOptions);
            return moves;
        });

        self.alphaBeta.setBeforeRecursiveCall((move) => {

            if (self.log.search) {
                let sanMove = Darkchess.getSAN(move);
                console.log('\n\n\tMove', sanMove);
            }

            self.darkchess.move(move);
            //updateOpponentPieceInfoForSearch(move, true);
        });

        self.alphaBeta.setAfterRecursiveCall((move) => {
            self.darkchess.undoMove(move);
            //updateOpponentPieceInfoForSearch(move, false);
        });

        self.alphaBeta.setBeforeNoneItemsRecursiveCall(() => {
            self.darkchess.swapPlayer();
        });

        self.alphaBeta.setAfterNoneItemsRecursiveCall(() => {
            self.darkchess.swapPlayer();
        });

        self.alphaBeta.setBeforeRecursiveQuiesceCall((move) => {
            self.darkchess.move(move);
            //updateOpponentPieceInfoForSearch(move, true);
        });

        self.alphaBeta.setAfterRecursiveQuiesceCall((move) => {

            self.darkchess.undoMove(move);
            //updateOpponentPieceInfoForSearch(move, false);
        });

        let startMaximizing = (self.color === Darkchess.WHITE);

        //  TESTING opponent piece info, assuring no change when searching.
        let pieceTypes = [
            Darkchess.KING,
            Darkchess.QUEEN,
            Darkchess.ROOK,
            Darkchess.BISHOP,
            Darkchess.KNIGHT,
            Darkchess.PAWN
        ];

        let beforeInfo = [];
        for (let i = 0; i < pieceTypes.length; i++) {
            let type = pieceTypes[i];

            let visible = self.opponentPieceInfo[type][Darkchess.VISIBLE];
            let hidden = self.opponentPieceInfo[type][Darkchess.HIDDEN];

            if (type === Darkchess.BISHOP) {

                let bishopTypes = [Darkchess.LIGHT, Darkchess.DARK];
                for (let j = 0; j < bishopTypes.length; j++) {
                    let bishopType = bishopTypes[j];
                    visible = self.opponentPieceInfo[type][bishopType][Darkchess.VISIBLE];
                    hidden = self.opponentPieceInfo[type][bishopType][Darkchess.HIDDEN];

                    beforeInfo.push(visible);
                    beforeInfo.push(hidden);
                }

            } else {
                beforeInfo.push(visible);
                beforeInfo.push(hidden);
            }
        }

        if (false) logOpponentPieceInfo('Before search');


        //let start = new Date();
        let moves = self.alphaBeta.search(startMaximizing);
        //let end = new Date();
        //let elapsed = Utils.elapsed(start, end);
        //console.log('\n\tSearched time\n\tElapsed: ' + elapsed);

        if (false) logOpponentPieceInfo('After search');

        if (moves.length === 0) {
            self.darkchess.logBoard();
        }

        let selectedMove = selectMove(moves);

        let afterInfo = [];
        for (let i = 0; i < pieceTypes.length; i++) {
            let type = pieceTypes[i];

            let visible = self.opponentPieceInfo[type][Darkchess.VISIBLE];
            let hidden = self.opponentPieceInfo[type][Darkchess.HIDDEN];

            if (type === Darkchess.BISHOP) {

                let bishopTypes = [Darkchess.LIGHT, Darkchess.DARK];
                for (let j = 0; j < bishopTypes.length; j++) {
                    let bishopType = bishopTypes[j];
                    visible = self.opponentPieceInfo[type][bishopType][Darkchess.VISIBLE];
                    hidden = self.opponentPieceInfo[type][bishopType][Darkchess.HIDDEN];

                    afterInfo.push(visible);
                    afterInfo.push(hidden);
                }

            } else {
                afterInfo.push(visible);
                afterInfo.push(hidden);
            }
        }

        for (let i = 0; i < afterInfo.length; i++) {
            Utils.assert(beforeInfo[i] === afterInfo[i], 'opponentPieceInfo: Search changed # of ' + i);
        }

        if (false) logOpponentPieceInfo('Before moving ' + Darkchess.getSAN(selectedMove));

        self.darkchess.move(selectedMove);

        let knewInViewInfo = inViewInfo;
        let knowInViewInfo = self.darkchess.getInView(self.color);
        updateOpponentPieceInfo(selectedMove, knewInViewInfo, knowInViewInfo);

        if (false) logOpponentPieceInfo('After moving ' + Darkchess.getSAN(selectedMove));

        self.alphaBeta.setAssessRisk(null);
        self.alphaBeta.setEvaluate(null);
        self.alphaBeta.setReturnConditionCall(null);
        self.alphaBeta.setGetItems(null);
        self.alphaBeta.setGetQuiesceItems(null);
        self.alphaBeta.setBeforeRecursiveCall(null)
        self.alphaBeta.setAfterRecursiveCall(null);
        self.alphaBeta.setBeforeNoneItemsRecursiveCall(null);
        self.alphaBeta.setAfterNoneItemsRecursiveCall(null);
        self.alphaBeta.setBeforeRecursiveQuiesceCall(null);
        self.alphaBeta.setAfterRecursiveQuiesceCall(null);

        return selectedMove;
    }

    function selectMove(moves) {
        Utils.assert(moves.length > 0, 'No moves to select?');

        if (moves.length === 1) return moves[0].move;

        if (self.onlyBest) {
            return moves[moves.length - 1].move;
        }

        let decimals = 4;
        let scores = moves.map((o) => {
            return o.score.toFixed(decimals) * 1;
        });

        let normalizedScores = Utils.normalize2(scores, decimals);

        if (false) {

            let test = normalizedScores.map((score, index) => {
                let scaleFactor = ((index + 1) / normalizedScores.length);

                let result = (score * scaleFactor).toFixed(decimals) * 1;

                return result;
            });

            Utils.printArray(test);
            console.log('sum', Utils.sum(test));

            let ttt = Utils.normalize2(test, decimals);
            Utils.printArray(ttt);

            let prevExtra = 0;
            let currentExtra = 0;
            let test2 = normalizedScores.map((score, index) => {
                let scaleFactor = ((index + 1) / normalizedScores.length);

                let result = (score * scaleFactor).toFixed(decimals) * 1;
                currentExtra = score - result;

                result += prevExtra;
                prevExtra = currentExtra;
                return result.toFixed(decimals) * 1;
            });

            Utils.printArray(test2);
            console.log('sum', Utils.sum(test2));

        }

        let seed = self.randomGenerator.random();
        let selectIndex = normalizedScores.length - 1;
        let threshold = 0.0;

        for (let i = 0; i < normalizedScores.length; i++) {
            let distribution = normalizedScores[i];
            threshold += distribution;

            if (seed <= threshold) {
                selectIndex = i;
                break;
            }
        }

        return moves[selectIndex].move;
    }

    function logOpponentPieceInfo(title) {
        console.log('\n\t' + title);
        console.log('King           ',  self.opponentPieceInfo[Darkchess.KING]);
        console.log('Queens         ',  self.opponentPieceInfo[Darkchess.QUEEN]);
        console.log('Rook           ',  self.opponentPieceInfo[Darkchess.ROOK]);
        console.log('Bishop (light) ',  self.opponentPieceInfo[Darkchess.BISHOP][Darkchess.LIGHT]);
        console.log('Bishop (dark)  ',  self.opponentPieceInfo[Darkchess.BISHOP][Darkchess.DARK]);
        console.log('Knight         ',  self.opponentPieceInfo[Darkchess.KNIGHT]);
        console.log('Pawns          ',  self.opponentPieceInfo[Darkchess.PAWN]);
    }

    function registerMove(move) {

        if (false) {
            self.darkchess.logBoard();
            logOpponentPieceInfo('REGISTER (before)\t' + Darkchess.getSAN(move));
        }

        let opponentColor = Darkchess.swapColor(self.color);
        let prevInViewInfo = self.darkchess.getInView(opponentColor);

        //  Get pieces 'previously' in view, before executing the move.
        let prevPiecesInView = getVisiblePieces(prevInViewInfo);

        self.darkchess.move(move);

        //  Get pieces currently in view.
        let inViewInfo = self.darkchess.getInView();
        let piecesInView = getVisiblePieces(inViewInfo);

        updateOpponentPieceInfoRegister(move, prevInViewInfo, inViewInfo);

        let knownPromotion = false;
        let isPromotion = (move.flags & Darkchess.BITS.PROMOTION);
        if (isPromotion) {

            if (move.piece !== Darkchess.PAWN) Utils.assert(false);

            // Seeing both the pawn and its destination (know the promotion!);
            if (inViewInfo[move.to] && prevInViewInfo[move.from]) {

                knownPromotion = true;

                if (move.promotion === Darkchess.BISHOP) {

                    let squareColor = Darkchess.squareColor(move.to);
                    self.opponentPieceInfo[move.promotion][squareColor][Darkchess.VISIBLE]++;

                } else {

                    self.opponentPieceInfo[move.promotion][Darkchess.VISIBLE]++;

                }

                self.opponentPieceInfo[Darkchess.PAWN][Darkchess.VISIBLE]--;

                if (self.opponentPieceInfo[Darkchess.PAWN][Darkchess.VISIBLE] < 0) {
                    self.opponentPieceInfoError = true;
                }

            } else if (inViewInfo[move.to]) {

                // Even though we observe only the destination, and we know it does not exist more
                // of the piece appearing, means it HAS to be a pawn promotion!
                if (self.opponentPieceInfo[move.promotion][Darkchess.HIDDEN] === 0) {

                    knownPromotion = true;

                    if (move.promotion === Darkchess.BISHOP) {
                        let squareColor = Darkchess.squareColor(move.to);
                        self.opponentPieceInfo[move.promotion][squareColor][Darkchess.VISIBLE]++;
                    } else {
                        self.opponentPieceInfo[move.promotion][Darkchess.VISIBLE]++;
                    }

                    // move.piece;
                    self.opponentPieceInfo[Darkchess.PAWN][Darkchess.HIDDEN]--;

                } else {

                    // We have no reason to belive it is a pawn promotion as long there are existing
                    // pieces hidding in the dark.
                    if (move.promotion === Darkchess.BISHOP) {
                        let squareColor = Darkchess.squareColor(move.to);
                        self.opponentPieceInfo[move.promotion][squareColor][Darkchess.VISIBLE]++;
                        self.opponentPieceInfo[move.promotion][squareColor][Darkchess.HIDDEN]--;
                    } else {
                        self.opponentPieceInfo[move.promotion][Darkchess.VISIBLE]++;
                        self.opponentPieceInfo[move.promotion][Darkchess.HIDDEN]--;
                    }
                }
            }
        }

        assureCorrectOpponentPieceInfo('Register Move (' + Darkchess.getSAN(move) + ', ' + move.captured + ')');

        if (false) {
            self.darkchess.logBoard();
            logOpponentPieceInfo('REGISTER (after)\t' + Darkchess.getSAN(move));
        }
    }

    function getOpponentPieceInfoError() {
        return self.opponentPieceInfoError;
    }

    function assureCorrectOpponentPieceInfo(message) {

        let pieceTypes = [
            Darkchess.KING,
            Darkchess.QUEEN,
            Darkchess.ROOK,
            Darkchess.BISHOP,
            Darkchess.KNIGHT,
            Darkchess.PAWN
        ];

        for (let i = 0; i < pieceTypes.length; i++) {
            let pieceType = pieceTypes[i];

            let visiblePieces = self.opponentPieceInfo[pieceType][Darkchess.VISIBLE];
            let hiddenPieces = self.opponentPieceInfo[pieceType][Darkchess.HIDDEN];

            if (pieceType === Darkchess.BISHOP) {
                visiblePieces = self.opponentPieceInfo[pieceType][Darkchess.LIGHT][Darkchess.VISIBLE];
                hiddenPieces = self.opponentPieceInfo[pieceType][Darkchess.DARK][Darkchess.HIDDEN];
            }

            if (visiblePieces < 0) {
                self.opponentPieceInfoError = true;
            }

            if (hiddenPieces < 0) {
                self.opponentPieceInfoError = true;
            }

            let totalPieces = visiblePieces + hiddenPieces;

            if (pieceType === Darkchess.KING) {
                Utils.assert(totalPieces < 2, message + '\tOpponentPieceInfo: More then 1 king?');

            } else if (pieceType === Darkchess.PAWN) {
                Utils.assert(totalPieces < 9, message + '\tOpponentPieceInfo: More then 9 pawns?');

            }

            Utils.assert(totalPieces < 11, message + '\tOpponentPieceInfo: More then 11 ' + pieceType + '\'s');
        }
    }

    function evaluate() {

        let inViewInfo = self.darkchess.getInView();
        let opponentColor = Darkchess.swapColor(self.color);

         let fen = self.darkchess.getFen();
         return self.evaluate(fen, opponentColor, inViewInfo, self.opponentPieceInfo);
    }

    return {
        log: log,

        setColor: setColor,
        setState: setState,
        setRandomGenerator: setRandomGenerator,
        enableSearchResultLog: enableSearchResultLog,
        enableSearchLog: enableSearchLog,
        enableRisk: enableRisk,
        setSearchDepth: setSearchDepth,
        onlyBestmoves: onlyBestmoves,
        sortMoves: sortMoves,
        setRiskFactor: setRiskFactor,

        setRating: setRating,
        addRating: addRating,
        getRating: getRating,
        getPlayerColor: getPlayerColor,
        getPlayerId: getPlayerId,
        getOpponentPieceInfoError: getOpponentPieceInfoError,
        getInView: getInView,

        registerMove: registerMove,
        move: move,
        evaluate: evaluate,

        logOpponentPieceInfo: logOpponentPieceInfo,
        resetOpponentPieceInfo: resetOpponentPieceInfo
    };
}

module.exports = Agent;
