'use strict'

var CONST = require('./const');

function _getTimestamp() {
    var timestamp = (new Date()).toJSON().split('.')[0];
    return timestamp.replace('T', ' ');
}

function _assert(expression, message) {
    if (!expression) {
        console.error('\nAssertion: ', message, '\n');
        undefined.assert;
    }
}

function _elapsed(fromDate, toDate) {
    let ms = toDate.getTime() - fromDate.getTime();

    let milli   = (ms % 1000);
    let seconds = (Math.floor(ms / 1000) % 60);
    let minutes = (Math.floor(ms / (1000*60)) % 60);
    let hours   = (Math.floor(ms / (1000*60*60)) % 24);

    let hh = (hours   < 10)? '0' + hours: hours;
    let mm = (minutes < 10)? '0' + minutes: minutes;
    let ss = (seconds < 10)? '0' + seconds: seconds;

    let mi = '';
    if (milli < 100) mi += '0';
    if (milli < 10) mi += '0';
    mi += '' + milli;

    return (hh + ':' + mm + ':' + ss + '.' + mi);
}

function _getRandomNumber(min, max) {
    return Math.floor(Math.random() * (max-min+1) + min);
}

function _printArray(array, num) {

    if (typeof num === 'undefined') num = 0;

    let spaces = _genChar('   ', num);
    console.log('\t' + spaces + '[' + array.length + '] ------');

    for (let i=0; i<array.length; i++) {
        let item = array[i];

        if (Array.isArray(item)) {
            _printArray(item, num + 1);
        } else {
            console.log('\t' + spaces, item);
        }
    }
}

function _normalize(array, decimals, makePositive) {
    let max = Math.max.apply(null, array);
    let min = Math.min.apply(null, array);

    let result = array.map(function(item) {
        let numerator = (makePositive)? (item - min): (item - max);
        let denominator = max - min;

        let num = (numerator / denominator);
        return num.toFixed(decimals) * 1; // Cast back to number
    });
    return result;
}

function _normalize2(array, decimals) {

    let min = Math.min.apply(null, array);
    let mapped = array.map((item) => {
        return item + 2 * Math.abs(min);
    });

    let total = 0;
    for (let i = 0; i < mapped.length; i++) {
        total += mapped[i];
    }

    let normalized = [];
    for (let i = 0; i < mapped.length; i++) {
        normalized.push((mapped[i] / total).toFixed(decimals) * 1);
    }

    return normalized;
}

function _sum(array) {
    let sum = 0;
    for (let number of array) {
        sum += number;
    }
    return sum;
}

function _copy(data) {
    return JSON.parse(JSON.stringify(data));
}

//  Count occurences of a substring in a string
//  @author Vitim.us https://gist.github.com/victornpb/7736865
function _occurrences(sourceString, subString, allowOverlapping) {

    sourceString += '';
    subString += '';

    if (subString.length <= 0) return (sourceString.length + 1);

    let count = 0;
    let position = 0;
    let step = allowOverlapping? 1: subString.length;

    while (true) {
        position = sourceString.indexOf(subString, position);
        if (position >= 0) {
            ++count;
            position += step;
        } else break;
    }

    return count;
}

module.exports = {
    getTimestamp: _getTimestamp,
    assert: _assert,
    elapsed: _elapsed,
    getRandomNumber: _getRandomNumber,
    combinations: _combinations,

    occurrences: _occurrences,
    mirror2D: _mirror2D,

    sum: _sum,
    normalize: _normalize,
    normalize2: _normalize2,
    printArray: _printArray,
    copy: _copy,
};
