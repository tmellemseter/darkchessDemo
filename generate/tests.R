library(ggplot2)
library(scales)
library(RColorBrewer)
source('extra.R')

theme_set(theme_gray(base_size = 18))

convertResult <- function(fileData, type) {
	fileData[,5] <- sapply(fileData[,5], type)
	return(fileData)
}

loadFile <- function(filename) {
	directory <- paste(getwd(), '/tests/', sep="")
	filePath <- paste(directory, filename, sep="")
	fileData <- read.csv(filePath, skip=1, sep=",", head=TRUE, stringsAsFactors=FALSE)
	fileData <- convertResult(fileData, as.character)
	return(fileData)
}

plotRatingTrends <- function(f1, f2, f3, f4) {
	
	f1rt <- plotRatingTrend(f1)
	f2rt <- plotRatingTrend(f2)
	f3rt <- plotRatingTrend(f3)
	f4rt <- plotRatingTrend(f4)
	
	multiplot(f1rt, f2rt, f3rt, f4rt, cols=2)	
}

plotTest <- function(fileData) {

	a1 <- fileData[fileData$player == 'agent1',]
	a1LOS <- getLOS(a1)
	print(a1LOS)

	a2 <- fileData[fileData$player == 'agent2',]
	a2LOS <- getLOS(a2)
	print(a2LOS)

	theme_set(theme_gray(base_size = 18))
	pe <- plotE(fileData)
	pc <- plotC(fileData)
	pa <- plotA(fileData)
	
	multiplot(pc, pa, pe, cols=3)
}

plotKSmooth <- function(fileData) {

	a1 <- fileData[fileData$player == 'agent1',]
	a2 <- fileData[fileData$player == 'agent2',]
	
	cols <- c("#999999", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")
	#cols <- brewer.pal(4, "PuBuGn")

	plot(a1$id, a1$rating, type='l', xlab="Number of Games", ylab="Rating", ylim=c(300, 700))
 	lines(ksmooth(a1$id, a1$rating, "normal", bandwidth = 250), col=cols[4], lwd=3)
 	lines(ksmooth(a1$id, a1$rating, "normal", bandwidth = 10), col=cols[3], lwd=3)
 	lines(ksmooth(a1$id, a1$rating, "normal", bandwidth = density(a1$id)$bw), col=cols[7], lwd=3)

	legend("topright", c('Overfitting','Underfitting','Density'),
		 lty=c(1,1,1), lwd=c(2.5,2.5), col=c(cols[4],cols[3],cols[7]),
		 title = "Line models", inset = .02)
}

plotC <- function(fileData) {
	
	tbl <- table(fileData$color, fileData$result)
	
	testData <- chisq.test(tbl)
	print(testData)

	fileData <- convertResult(fileData, as.integer)
	tgc <- summarySE(fileData, measurevar="result", groupvars="color",  conf.interval=.999)

	plt <- ggplot(tgc, aes(x=c("Black","White"), y=result, fill=color, label=result, width=0.6)) +
			guides(fill=FALSE) +
			geom_bar(stat="identity") +
			geom_errorbar(aes(ymin=result-ci, ymax=result+ci), width=.2, position=position_dodge(.9)) +
			scale_x_discrete("Player Color") +
			scale_y_continuous("Win Ratio", limits = c(0, 1.00)) +
			geom_text(size = 5, position = position_stack(vjust = 0.5)) +
			ggtitle("Color Win Ratio")
	return(plt)

}

plotError <- function(fileData) {

	plt <- ggplot(fileData, aes('error', fill=error)) +
			geom_bar(position="stack") +
			xlab("Error") +
			ylab("Number of Games") +
			ggtitle("Succeeded games") +
			scale_fill_discrete(name="Error") +
			facet_grid(~ player)
	return(plt)

}

plotE <- function(fileData) {
	
	a1 <- fileData[fileData$player == 'agent1',]
	a2 <- fileData[fileData$player == 'agent2',]

	n <- nrow(a1)
	a1Errors <- nrow(a1[a1$error == 'true',])
	a2Errors <- nrow(a2[a2$error == 'true',])

	a1Ratio <- a1Errors / n;
	a2Ratio <- a2Errors / n;

	players <- c('Agent1', 'Agent1', 'Agent2', 'Agent2')
	aLabels <- c('Error', 'Success', 'Error', 'Success')
	ratios <- c(a1Ratio, 1-a1Ratio, a2Ratio, 1-a2Ratio)

	df <- data.frame(players, aLabels, ratios)

	plt <- ggplot(df, aes(x=players, ratios, fill=aLabels, order=ratios, label=ratios)) +
			geom_bar(stat="identity", position=position_stack(reverse = TRUE)) +
			scale_fill_discrete(guide=guide_legend("", reverse = TRUE)) +
			scale_x_discrete("Agent") +
			scale_y_continuous("Error Ratio") +
			ggtitle("Error in Evaluation") +
			geom_text(size = 5, position = position_stack(vjust = 0.5, reverse = TRUE))

	return(plt)
}

ma <- function(x,n=5){
	filter(x,rep(1/n,n), sides=2)
}

plotT3 <- function(fileData, only) {
	
	a1 <- fileData[fileData$player == 'agent1',]
	
	portion <- a1[a1$id < only,]

	colours <- rainbow(10)
	plot(portion$id, portion$rating, type='p')
 	lines(ksmooth(portion$id, portion$rating, "normal", bandwidth = density(portion$id)$bw), col=colours[2], lwd=3)	
}

plotA <- function(fileData) {
	tbl <- table(fileData$player, fileData$result)
	
	testData <- chisq.test(tbl)
	print(testData)

	fileData <- convertResult(fileData, as.integer)
	tgc <- summarySE(fileData, measurevar="result", groupvars="player",  conf.interval=.999)

	ggplot(tgc, aes(x=c("Agent1","Agent2"), y=result, fill=player, label=result, width=0.6)) +
		guides(fill=FALSE) +
		geom_bar(stat="identity") +
		geom_errorbar(aes(ymin=result-ci, ymax=result+ci), width=.2, position=position_dodge(.9)) +
		scale_x_discrete("Agent") +
		scale_y_continuous("Win Ratio", limits = c(0, 1.00)) +
		geom_text(size = 5, position = position_stack(vjust = 0.5)) +
		ggtitle("Agent Win Ratio")
}

plotCA <- function(fileData) {

	fileData <- convertResult(fileData, as.integer)
	tgc <- summarySE(fileData, measurevar="result", groupvars=c("player", "color"),  conf.interval=.999)

	ggplot(tgc, aes(x=player, y=result, fill=color)) +
		geom_bar(stat="identity", position="dodge", color="black", size=.3) +
		geom_errorbar(aes(ymin=result-ci, ymax=result+ci),
				  width=.2, 
				  position=position_dodge(.9), 
				  size=.3) +
		scale_fill_hue(name="Color", breaks=c("b","w"), labels=c("Black","White")) +
		scale_x_discrete("Agent") +
		scale_y_continuous("Win ratio")


}

plotResultDifference <- function(fileData) {
	
	levels(fileData$color) <- c("Black", "White")

	ggplot(fileData, aes(color, fill=result)) +
		geom_bar(position="dodge") +
		xlab("Player Color") +
		ylab("Number of Games") +
		facet_grid(~ player) + 
		ggtitle("Game Results")
}

getA1 <- function(fileData) {
	a1 <- fileData[fileData$player == 'agent1',]
	return(a1)
}

plotQQ2x2 <- function(a1,a2,a3,a4) {

	par(mfrow=c(2,2))
	qqnorm(a1$rating, main = "No features set", ylim=c(400,600))
	qqnorm(a2$rating, main = "Risk Assessment", ylim=c(400,600))
	qqnorm(a3$rating, main = "Move Ordering", ylim=c(400,600))
	qqnorm(a4$rating, main = "Move Ordering and Risk Assessment", ylim=c(400,600))
}

plotQQ2x3 <- function(a1,a2,a3,a4,a5,a6) {

	ylimit <- c(0,750)

	par(mfrow=c(2,3))
	qqnorm(a1$rating, main = "Risk - No features", ylim=ylimit, xlab="")
	qqnorm(a2$rating, main = "Move Ordering - No features", ylim=ylimit, xlab="", ylab="")
	qqnorm(a3$rating, main = "Move Ordering - Risk", ylim=ylimit, xlab="", ylab="")
	qqnorm(a4$rating, main = "Move Ordering & Risk - No features", ylim=ylimit)
	qqnorm(a5$rating, main = "Move Ordering & Risk - Risk", ylim=ylimit, ylab="")
	qqnorm(a6$rating, main = "Move Ordering & Risk - Move Ordering", ylim=ylimit, ylab="")
}

logSapiro <- function(dataVector) {
	
	shapiro.test(dataVector)
}

logWilcoxon <- function(fileData) {
	
	wilcox.test(fileData[fileData$player == 'agent1',]$result, fileData[fileData$player == 'agent2',]$result, data=fileDatta)
	
}

plotColorDifference <- function(fileData) {
	
	ggplot(fileData, aes(fileData$color, fill=result)) +
		geom_bar(position="dodge") +
		xlab("Player Color") + 
		ylab("Number of Games") +
		ggtitle("White vs Black")

}

plotWinDifference <- function(fileData) {
	a1 <- fileData[fileData$player == 'agent1',]
	a2 <- fileData[fileData$player == 'agent2',]

	nGames <- nrow(a1)

	a1wr <- getWinRatio(a1, nGames)
	a2wr <- getWinRatio(a2, nGames)

	ratios <- c(a1wr, a2wr)
	df <- data.frame(ratios)

	ggplot(df, aes(x=c("Agent1", "Agent2"), y=df, fill=c("blue", "red"))) +
		geom_bar(stat="identity") +
		scale_y_continuous("Win ratio") +
		scale_x_discrete("Agents") +
		scale_fill_discrete(guid=FALSE)
}

plotWinDifference2 <- function(fileData) {
	a1 <- fileData[fileData$player == 'agent1',]
	a1Ratio <- getColorWinRatio(a1)

	a2 <- fileData[fileData$player == 'agent2',]
	a2Ratio <- getColorWinRatio(a2)
	
	ratios <- c(a1Ratio, a2Ratio)

	players <- c('agent1','agent1','agent2','agent2')
	colors <- c('w','b','w','b')
	
	df <- data.frame(players, colors, ratios)

	ggplot(df, aes(x=players, y=ratios, fill=colors)) +
		geom_bar(stat="identity", position="dodge")
}

getColorWinRatio <- function(agent) {

	numberOfGames <- nrow(agent)

	white <- agent[agent$color == 'w',]
	whiteRatio <- getWinRatio(white, numberOfGames)

	black <- agent[agent$color == 'b',]
	blackRatio <- getWinRatio(black, numberOfGames)
	
	agentRatio <- c(whiteRatio, blackRatio)
	return(agentRatio)
}

getWinRatio <- function(resultData, numberOfGames) {
	
	wins <- nrow(resultData[resultData$result == 1,])
	draws <- nrow(resultData[resultData$result == 0.5,])

	ratio <- (wins + (0.5 * draws)) / numberOfGames
	return(ratio)
}

plotRatingChange <- function(fileData) {

	a1 <- fileData[fileData$player == 'agent1',]
	a2 <- fileData[fileData$player == 'agent2',]

	tk <- c("id","player","rating")
	tp <- fileData[tk]

	a1kline <- ksmooth(a1$id, a1$rating, "normal", bandwidth = density(a1$id)$bw)
	tp[tp$player == 'agent1',4] <- a1kline$y

	a2kline <- ksmooth(a2$id, a2$rating, "normal", bandwidth = density(a2$id)$bw)
	tp[tp$player == 'agent2',4] <- a2kline$y

	a1ma <- ma(a1$rating, 10)
	tp[tp$player == 'agent1',5] <- a1ma

	a2ma <- ma(a2$rating, 10)
	tp[tp$player == 'agent2',5] <- a2ma

	a1mean <- mean(a1$rating)
	print(a1mean)
	a2mean <- mean(a2$rating)
	print(a2mean)

	ggplot(tp, aes(id, group=player)) +
		geom_line(aes(y=rating), size=1, color='grey') +
		stat_smooth(aes(y=V5, color="Moving Average"), size=2,level = 0.999) +
		geom_smooth(aes(y=rating, color="GAM"), size=2, se=FALSE) +
		stat_smooth(aes(y=V4, color="Nadaraya�Watson"), size=1.5, se=FALSE) +
		scale_colour_brewer(palette="Dark2") +
		labs(title = "Regression Analysis", color = "Method") +
		geom_hline(yintercept = a2mean) +
		geom_vline(xintercept = 130) +
#		ylim(250,750) +
		xlab("Number of Games") +
		ylab("Rating")
#		facet_grid(~player)
}

plotRatingTest <- function(agent) {

#	arm <- NLSstAsymptotic(sortedXyData(expression(id),expression(rating),agent))
	
#	b0 <- arm[1]
#	print(b0)
#	b1 <- arm[2]
#	print(b1)
#	lrc <- arm[3]
#	print(lrc)
#	plt <- ggplot(agent, aes(id, rating, color=result)) +
#			geom_point() + 
#			stat_smooth(method = 'loess', formula = y ~ x)
#			stat_smooth(method = 'nls', formula = y ~ a + b * (1 - exp(-exp(c) * x)), method.args=list(start=c(a=b0,b=b1,c=lrc)),color = 'black')	
#	return(plt)

}

plotRatingTrend <- function(fileData) {

	a1 <- getA1(fileData)
	m <- mean(a1$rating)

	rt <- ggplot(fileData, aes(id, rating, group=player)) +
#			geom_point() +
			geom_line() +
			stat_smooth() +
			geom_hline(yintercept = m) + 
#    			stat_smooth(method = 'lm', aes(id, rating, color=player), se = FALSE) +
#    			stat_smooth(method = 'lm', formula = y ~ x, 			aes(id, rating, color='Linear'), se = FALSE) +
#    			stat_smooth(method = 'lm', formula = y ~ poly(x,2),		aes(id, rating, color='Polynomial'), se = FALSE) +
#    			stat_smooth(method = 'nls', formula = y ~ a * log(x) + b,	aes(id, rating, color='Logaritmic'), se = FALSE, method.args = list(start=c(a=1,b=1))) +
			ylim(250,750) +
			ggtitle('Rating Trend') +
#			scale_y_continuous("Rating", breaks= pretty_breaks()) +
			scale_x_continuous("Number of Games")
	return(rt)
}

plotWinRatios <- function(fileData) {
	
	white <- fileData[fileData$color == 'w',]
	black <- fileData[fileData$color == 'b',]

	df2 <- getColorWinRatio(fileData)
	print(df2)

	ggplot(df2, aes(x=c("White", "Black"), y=df, fill=c("blue", "red"))) +
		geom_bar(stat="identity") +
		scale_y_continuous("Win ratio") +
		scale_x_discrete("Player color") +
		scale_fill_discrete(guid=FALSE)

}

getLOS <- function(fileData) {
	
	nGames <- nrow(fileData)

	nWins <- nrow(fileData[fileData$result == 1,])
	nDraws <- nrow(fileData[fileData$result == 0.5,])
	nLost <- nrow(fileData[fileData$result == 0,])

	winRatio <- (nWins + (0.5 * nDraws)) / nGames
#	drawRatio <- nDraws / nGames

#	eloDifference <- -log(1 / winRatio - 1) * 400 / log(10)
#	print(eloDifference)
	
	los <- 0.5 * ( 1 + erf( (nWins - nLost) / sqrt(2 * (nWins + nLost))))
	return(los)
}

erf <- function(x) {
	result <- 2 * pnorm(x * sqrt(2)) - 1
	return(result)
}

getUserTestResults <- function() {

	id	 <- c( 1    , 1   , 2    , 2    , 3    , 3    , 4    , 4    , 5    , 5    , 6    , 6    , 7    , 7    , 8    , 8    , 9    , 9    , 10   , 10   , 11   , 11   , 12   , 12   )
	player <- c( 'AI' , 'P' , 'AI' , 'P'  , 'AI' , 'P'  , 'AI' , 'P'  , 'AI' , 'P'  , 'AI' , 'P'  , 'AI' , 'P'  , 'AI' , 'P'  , 'AI' , 'P'  , 'AI' , 'P'  , 'AI' , 'P'  , 'AI' , 'P'  )
	color	 <- c( 'b'  , 'w' , 'w'  , 'b'  , 'w'  , 'b'  , 'b'  , 'w'  , 'b'  , 'w'  , 'w'  , 'b'  , 'w'  , 'b'  , 'b'  , 'w'  , 'b'  , 'w'  , 'w'  , 'b'  , 'w'  , 'b'  , 'b'  , 'w'  )
	result <- c( 1	, 0	, 1	 , 0	  , 0	   , 1    , 1    , 0	, 1	 , 0    , 0    , 1    , 1    , 0    , 0    , 1    , 0    , 1    , 1    , 0    , 0    , 1    , 0    , 1    )

	df <- data.frame(id, player, color, result)

	return(df)
}

plotWinRatioUT <- function() {

	results <- getUserTestResults()

	ai <- results[results$player == 'AI',]
	nGames <- nrow(ai)
	print(ai)

	aiw		<- ai[ai$color == 'w',]
	aiwwins	<- nrow(aiw[aiw$result == 1,])
	aiwwr		<- aiwwins / nGames

	aib		<- ai[ai$color == 'b',]
	aibwins	<- nrow(aib[aib$result == 1,])
	aibwr		<- aibwins / nGames

	p  <- results[results$player == 'P',]

	pw		<- p[p$color == 'w',]
	pwwins	<- nrow(pw[pw$result == 1,])
	print(pwwins)
	pwwr		<- pwwins / nGames

	pb		<- p[p$color == 'b',]
	pbwins	<- nrow(pb[pb$result == 1,])
	pbwr		<- pbwins / nGames

	ratios <- c(aiwwr, aibwr, pwwr, pbwr)
	player <- c('AI', 'AI', 'P', 'P')
	color <- c('w', 'b', 'w', 'b')

	df <- data.frame(player, color, ratios)
	print(df)

#	ggplot(df, aes(x=c("AI", "Participants"), y=df, fill=c("blue", "red"))) +
#		geom_bar(stat="identity") +
#		scale_y_continuous("Win ratio") +
#		scale_x_discrete("Players") +
#		scale_fill_discrete(guid=FALSE)
	
}

