'use strict'

let XorShift = require('xorshift').constructor;
let fs = require('fs');

let Utils = require('../server/utils');
let Darkchess = require('../server/darkchess');
let Agent = require('../server/ais/agent2');

let input = process.argv[2];
let newFilename = process.argv[3];

if (typeof input === 'undefined') {
    console.log('Missing parameter! First parameter needs to be either:');
    console.log('* The number of games to be played');
    console.log('* A filename located in the same directory for redoing the test');
    console.log('\nOPTIONAL second param');
    console.log(
        'By default, the results are written to file "TEST-000", where 000 is the file ID. ' +
        'By specifying the second parameter will replace the default file ID. ' +
        'For example, specifying 20 as the second parameter, will create the file TEST-20'
    );

    return;
}

let game = {};
let filename = 'test-0000';

let generateNewData = parseInt(input);

if (typeof newFilename === 'undefined') {
    filename = (generateNewData)? 'test-0000': 'retest-0000';
} else {
    filename = newFilename;
}

console.log('\n\tInput: ' + input);
console.log('\tSave to: ' + filename);

if (generateNewData) {

    start(generateNewData);

} else {
    console.log('\n\tReading from file "' + input + '.csv"');
    let fileContent = '';

    fs.readFile(__dirname + '/tests/' + input + '.csv', 'utf8', (err, data) => {
        if (err) {
            return console.log(err);
        }

        let numberOfGames = Utils.occurrences(data, '\n');

        // The seed and title row.
        numberOfGames -= 2;

        // one row per player.
        numberOfGames /= 2;

        start(numberOfGames, data);
    });
}

function play(playerToMove, opponent) {

    let move = playerToMove.move();
    opponent.registerMove(move);

    game.move(move);
    game.swapPlayer();

    if (game.isGameOver()) {

        let isDraw = game.isDraw();
        let isKingCaptured = game.isKingCaptured();

        let insufficientMaterial = game.isInsufficientMaterial();
        let fiftyMoveRule = game.is50MoveRule();

        return {
            isWinner: isKingCaptured,
            winner: playerToMove.getPlayerColor(),
            isDraw: isDraw,
            isInsufficientMaterial: insufficientMaterial,
            isFiftyMoveRule: fiftyMoveRule,
        };
    } else {
        return play(opponent, playerToMove);
    }
}

function elapsed(fromDate, toDate) {
    let ms = toDate.getTime() - fromDate.getTime();

    let milli   = (ms % 1000);
    let seconds = (Math.floor(ms / 1000) % 60);
    let minutes = (Math.floor(ms / (1000*60)) % 60);
    let hours   = (Math.floor(ms / (1000*60*60)) % 24);

    let hh = (hours   < 10)? '0' + hours: hours;
    let mm = (minutes < 10)? '0' + minutes: minutes;
    let ss = (seconds < 10)? '0' + seconds: seconds;

    let mi = '';
    if (milli < 100) mi += '0';
    if (milli < 10) mi += '0';
    mi += '' + milli;

    return (hh + ':' + mm + ':' + ss + '.' + mi);
}

function start(numberOfGames, file) {

    let seedInfo = '';
    let readFromFile = (typeof file !== 'undefined');
    if (readFromFile) {
        let rows = file.split('\n');
        seedInfo = rows.shift();
    }

    let randomSeed = [];
    if (readFromFile) {
        randomSeed = seedInfo.split('-');

    } else {
        let max = (Math.pow(2, 32) - 1);
        let r1 = Utils.getRandomNumber(0, max);
        let r2 = Utils.getRandomNumber(0, max);
        let r3 = Utils.getRandomNumber(0, max);
        let r4 = Utils.getRandomNumber(0, max);
        randomSeed = [r1, r2, r3, r4];
    }

    let randomGenerator = new XorShift(randomSeed);

    let header = randomSeed[0] + '-' + randomSeed[1] + '-' + randomSeed[2] + '-' + randomSeed[3];
    let fileContent = header + '\n';
    fileContent += 'id,player,color,result,estimate,rating,error,comment\n';

    let playerId1 = 'agent1';
    let player1 = new Agent(playerId1);
    player1.setRandomGenerator(randomGenerator);
    player1.setRating(500);
    player1.setSearchDepth(2);
    //player1.onlyBestmoves();
    //player1.enableRisk();
    //player1.sortMoves();
    //player1.setRiskFactor(0.5);

    let playerId2 = 'agent2';
    let player2 = new Agent(playerId2);
    player2.setRandomGenerator(randomGenerator);
    player2.setRating(500);
    player2.setSearchDepth(2);
    //player2.onlyBestmoves()
    //player2.enableRisk();
    //player2.sortMoves();
    //player2.setRiskFactor(0.5);

    game = new Darkchess();
    let ommitRateUpdateOnError = false;

    // The k value for ELO calculation
    let developmentCoefficient = 20;

    let start = new Date();

    for (let i = 0; i < numberOfGames; i++) {

        let player1Color = ((i % 2) === 0)? Darkchess.WHITE: Darkchess.BLACK;
        let player2Color = ((i % 2) === 0)? Darkchess.BLACK: Darkchess.WHITE;

        player1.setColor(player1Color);
        player1.resetOpponentPieceInfo();
        player1.setState(Darkchess.DEFAULT_POSITION);

        player2.setColor(player2Color);
        player2.resetOpponentPieceInfo();
        player2.setState(Darkchess.DEFAULT_POSITION);

        game.setState(Darkchess.DEFAULT_POSITION);

        let result = null;

        if (player1Color === Darkchess.WHITE) {
            result = play(player1, player2);
        } else {
            result = play(player2, player1);
        }

        let comment = '';
        if (result.isInsufficientMaterial) comment = 'Insufficient Material';
        else if (result.isFiftyMoveRule) comment = '50 move rule';

        let player1Result = 0.5;
        let player2Result = 0.5;

        if (result.isWinner) {
            if (result.winner === Darkchess.WHITE) {

                player1Result = (player1Color === Darkchess.WHITE)? 1: 0;
                player2Result = (player2Color === Darkchess.WHITE)? 1: 0;
            } else {

                player1Result = (player1Color === Darkchess.BLACK)? 1: 0;
                player2Result = (player2Color === Darkchess.BLACK)? 1: 0;
            }
        }

        let rating1 = player1.getRating();
        let rating2 = player2.getRating();
        let player1EstimateScore = estimatePlayer(rating1, rating2);
        let player2EstimateScore = estimatePlayer(rating2, rating1);

        let estRating1 = Math.round(rating1 + player1EstimateScore);
        let estRating2 = Math.round(rating2 + player2EstimateScore);

        let k = developmentCoefficient;

        // Points to adjust
        let player1Points = Math.round(k * (player1Result - player1EstimateScore));
        let player2Points = Math.round(k * (player2Result - player2EstimateScore));

        let player1Error = player1.getOpponentPieceInfoError();
        let player2Error = player2.getOpponentPieceInfoError();
        let error = (player1Error || player2Error);

        if (!ommitRateUpdateOnError || !error) {
            player1.addRating(player1Points);
            player2.addRating(player2Points);
        }

        let player1Elo = player1.getRating();
        let player2Elo = player2.getRating();

        let id = (i + 1);
        fileContent += id + ',' + playerId1 + ',' + player1Color + ',' + player1Result + ',' + estRating1 + ',' + player1Elo + ',' + player1Error + ',' + comment + '\n';
        fileContent += id + ',' + playerId2 + ',' + player2Color + ',' + player2Result + ',' + estRating2 + ',' + player2Elo + ',' + player2Error + ',' + comment + '\n';

        console.log('\t' + i);
    }

    let end = new Date();
    let elapsed = elapsed(start, end);
    console.log('\n\tGenerated ' + numberOfGames + ' games\n\tElapsed: ' + elapsed);

    if (numberOfGames > 0) {

        let directory = 'tests';
        mkdir(directory);
        writeFile(fileContent, directory, filename);
    }
}

function estimatePlayer(rating1, rating2) {
    let r1 = Math.pow(10, rating1 / 400);
    let r2 = Math.pow(10, rating2 / 400);
    return r1 / (r1 + r2);
}

function mkdir(name) {
    if (!fs.existsSync('./' + name)) {
        fs.mkdirSync('./' + name);
    }
}

function writeFile(content, directory, filename) {

    fs.writeFile(__dirname + '/' + directory + '/' + filename + '.csv', content, (e) => {
        if (e) {
            return console.log(e);
        }
        console.log('\tThe file "' + filename + '" has been created in "/' + directory + '"');
    })

}
